MobCombat
=========

Transforms teams of players into battling hostile mobs.

For details on installation, including permissions and configuration settings, refer to INSTALLATION.md.

For help on building MobCombat, refer to BUILDING.md.
