package io.github.totemo.mobcombat.schedule;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.World;

// --------------------------------------------------------------------------
/**
 * Represents the current phase of the moon.
 */
public enum MoonPhase
{
  FullMoon,
  WaningGibbous,
  LastQuarter,
  WaningCrescent,
  NewMoon,
  WaxingCrescent,
  FirstQuarter,
  WaxingGibbous;

  // --------------------------------------------------------------------------
  /**
   * Return the current phase of the moon.
   * 
   * @return the current phase of the moon.
   */
  public static MoonPhase getCurrent()
  {
    World world = Bukkit.getWorlds().get(0);
    long time = world.getFullTime();
    int dayNumber = (int) (time / 24000);
    return values()[dayNumber % values().length];
  }

  // --------------------------------------------------------------------------
  /**
   * Return the MoonPhase with the specified case-insensitive name.
   * 
   * @param name the name, case-insensitive.
   * @return the MoonPhase with the specified case-insensitive name.
   */
  public static MoonPhase getByName(String name)
  {
    return _byName.get(name.toLowerCase());
  }

  // --------------------------------------------------------------------------
  /**
   * Map from lower-case enum name to instance.
   */
  protected static HashMap<String, MoonPhase> _byName;
  static
  {
    for (MoonPhase phase : values())
    {
      _byName.put(phase.name().toLowerCase(), phase);
    }
  }
} // class MoonPhase