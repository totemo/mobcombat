package io.github.totemo.mobcombat.schedule;

// ----------------------------------------------------------------------------
/**
 * Manages scheduled transformations of players to and from mob form.
 */
public class MobSchedule
{
  // --------------------------------------------------------------------------
  /**
   * Perform scheduled transformations to and from mob form.
   */
  public void updateTime()
  {

  }

} // class MobSchedule