package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.mobs.MobType;

import java.util.Set;

import org.bukkit.entity.Player;

// ----------------------------------------------------------------------------
/**
 * The API used to drive the MobCombat plugin from other plugins.
 * 
 * Ignore the team aspects of the API - they're not currently implemented.
 */
public class API
{
  // --------------------------------------------------------------------------
  /**
   * Return the canonical name of the specified online player's mob type, e.g.
   * Enderman.
   * 
   * Players in human form will have the type "Human".
   * 
   * @param player the online player.
   * @return the canonical name of the specified online player's mob type, e.g.
   *         Enderman.
   */
  public String getMobType(Player player)
  {
    MobPlayerTracker tracker = _plugin.getMobPlayerTracker();
    MobPlayer mobPlayer = tracker.getMobPlayer(player);
    return mobPlayer.getMobType().getName();
  }

  // --------------------------------------------------------------------------
  /**
   * Set the mob type of the specified online player. Players in human form will
   * have the type "Human".
   * 
   * @param player the online player.
   * @param mobTypeName the case-insensitive mob type name; use "Human" to
   *          revert a player to human form.
   * @param showEffects if true, special effects are shown on transformation
   *          (lighting, particles).
   * @return true if the mobTypeName was valid and the player's mob type was
   *         set.
   */
  public boolean setMobType(Player player, String mobTypeName, boolean showEffects)
  {
    MobType mobType = _plugin.getMobTypeRegistry().getMobType(mobTypeName);
    if (mobType != null)
    {
      MobPlayerTracker tracker = _plugin.getMobPlayerTracker();
      MobPlayer mobPlayer = tracker.getMobPlayer(player);
      tracker.transformPlayer(mobPlayer, mobType, showEffects);
      return true;
    }
    else
    {
      return false;
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Return the set of canonical names of all enabled mob types.
   * 
   * @return the set of canonical names of all enabled mob types.
   */
  public Set<String> getAllMobTypes()
  {
    return _plugin.getMobTypeRegistry().getAllMobTypes();
  }

  // --------------------------------------------------------------------------
  /**
   * Return a random mob from the set defined in the configuration.
   * 
   * @return a random mob from the set defined in the configuration.
   */
  public String getRandomMobType()
  {
    return _plugin.getMobTypeRegistry().chooseRandomMobType().getName();
  }

  // --------------------------------------------------------------------------
  /**
   * Create a new team with the specified name.
   * 
   * If the specified team already exists, do nothing. A team must exist before
   * a player can be added to it.
   * 
   * @throws NullPointerException if team is null.
   */
  public void addTeam(String team)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * Remove the specified team.
   * 
   * Players that are currently disguised as mobs will be returned to human form
   * without cost or special effects.
   * 
   * @throws NullPointerException if team is null.
   */
  public void removeTeam(String team)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * Return an immutable set of all teams currently managed.
   * 
   * @return an immutable set of all teams currently managed.
   */
  public Set<String> getTeams()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * Assign the player to the specified team.
   * 
   * @param player the name of the player.
   * @param team the name of the team, or null to remove the player from all
   *          teams.
   */
  public void setTeam(String player, String team)
  {

  }

  // --------------------------------------------------------------------------
  /**
   * Constructor.
   * 
   * @param plugin the MobCombat plugin reference.
   */
  API(MobCombat plugin)
  {
    _plugin = plugin;
  }

  // --------------------------------------------------------------------------
  /**
   * Reference to the MobCombat plugin.
   */
  protected MobCombat _plugin;
} // class API