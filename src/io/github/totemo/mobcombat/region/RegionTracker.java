package io.github.totemo.mobcombat.region;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

// ----------------------------------------------------------------------------
/**
 * Manages regions loaded from the configuration.
 */
public class RegionTracker
{
  // --------------------------------------------------------------------------
  /**
   * Contructor.
   * 
   * @param plugin the owning plugin.
   */
  public RegionTracker(JavaPlugin plugin)
  {
    _plugin = plugin;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the name of the region containing the Location, or null if not
   * found.
   * 
   * Regions are assumed to not overlap.
   * 
   * @param loc the location.
   * @return the name of the region containing the Location, or null if not
   *         found.
   */
  public TransformationRegion getTransformationRegion(Location loc)
  {
    try
    {
      if (getWorldGuard() != null)
      {
        RegionManager rm = getWorldGuard().getRegionManager(loc.getWorld());
        HashMap<String, TransformationRegion> transRegions = _transformationRegions.get(loc.getWorld());
        if (rm != null && transRegions != null)
        {
          ApplicableRegionSet regions = rm.getApplicableRegions(loc);
          if (regions != null)
          {
            for (ProtectedRegion region : rm.getApplicableRegions(loc))
            {
              // Simply return the first match. I expect there to be no
              // overlapping transformation regions.
              TransformationRegion trans = transRegions.get(region.getId());
              if (trans != null)
              {
                return trans;
              }
            } // for all applicable regions
          }
        }
      }
    }
    catch (Exception ex)
    {
      _plugin.getLogger().severe(ex.getClass().getName() + " getting transformation region.");
    }
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if a player at the specified Location can use /become.
   * 
   * By default, a player is not allowed to use /become anywhere. In order to
   * use the command, he must be inside a WorldGuard region specified as
   * allowing /become in the configuration file and must not be in a higher
   * priority region specified in the configuration as denying /become.
   * 
   * For some reason, the WorldGuard "blocked-cmds" flag was not working for the
   * purpose of blocking the command. I suspect that is because WorldGuard is
   * listed as a dependency of MobCombat and MobCombat gets to process the
   * command before WorldGuard can veto it.
   * 
   * @param loc the location of the player.
   * @return true if a player at the specified Location can use /become.
   */
  public boolean canUseBecome(Location loc)
  {
    boolean allowed = false;
    try
    {
      if (getWorldGuard() != null)
      {
        RegionManager rm = getWorldGuard().getRegionManager(loc.getWorld());
        HashMap<String, Boolean> becomeAllows = _becomeRegions.get(loc.getWorld());
        int regionPriority = -1;
        if (rm != null && becomeAllows != null)
        {
          ApplicableRegionSet regions = rm.getApplicableRegions(loc);
          if (regions != null)
          {
            for (ProtectedRegion region : regions)
            {
              // Only use the /become allow flag of the highest priority region.
              if (region.getPriority() > regionPriority)
              {
                Boolean regionAllows = becomeAllows.get(region.getId());
                if (regionAllows != null)
                {
                  regionPriority = region.getPriority();
                  allowed = regionAllows;
                }
              }
            } // for all regions at the location
          }
        }
      }
    }
    catch (Exception ex)
    {
      _plugin.getLogger().severe(ex.getClass().getName() + " checking use of /become.");
    }
    return allowed;
  } // canUseBecome

  // --------------------------------------------------------------------------
  /**
   * Load all regions from the configuration file.
   * 
   * @param regionsSection the "regions" section of the configuration file.
   */
  public void load(ConfigurationSection regionsSection)
  {
    _transformationRegions.clear();
    _becomeRegions.clear();
    if (getWorldGuard() == null)
    {
      return;
    }

    ConfigurationSection transformationSection = regionsSection.getConfigurationSection("transformation");
    if (transformationSection != null)
    {
      loadTransformationRegions(transformationSection);
    }

    ConfigurationSection becomeSection = regionsSection.getConfigurationSection("become");
    if (becomeSection != null)
    {
      loadBecomeRegions(becomeSection);
    }
  } // load

  // --------------------------------------------------------------------------
  /**
   * Add the specified region.
   * 
   * @param region the region to track.
   */
  protected void addTransformatonRegion(TransformationRegion region)
  {
    HashMap<String, TransformationRegion> regionsForWorld = _transformationRegions.get(region.getWorld());
    if (regionsForWorld == null)
    {
      regionsForWorld = new HashMap<String, TransformationRegion>();
      _transformationRegions.put(region.getWorld(), regionsForWorld);
    }
    regionsForWorld.put(region.getName(), region);
  }

  // --------------------------------------------------------------------------
  /**
   * Return a cached reference to the WorldGuard plugin.
   * 
   * @return the WorldGuard plugin.
   */
  protected WorldGuardPlugin getWorldGuard()
  {
    if (_worldGuard == null)
    {
      _worldGuard = (WorldGuardPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
      if (_worldGuard == null)
      {
        _plugin.getLogger().severe("Can't get a reference to WorldGuard.");
      }
    }
    return _worldGuard;
  }

  // --------------------------------------------------------------------------
  /**
   * Load the regions where automatic transformations occur.
   * 
   * @param transformationSection the "regions.transformation" section of the
   *          configuration file.
   */
  public void loadTransformationRegions(ConfigurationSection transformationSection)
  {
    for (String worldName : transformationSection.getKeys(false))
    {
      World world = Bukkit.getWorld(worldName);
      if (world == null)
      {
        _plugin.getLogger().severe("Invalid world name " + worldName +
                                   " referenced defining automatic transformations.");
      }
      else
      {
        ConfigurationSection worldSection = transformationSection.getConfigurationSection(worldName);
        for (String regionName : worldSection.getKeys(false))
        {
          TransformationRegion region = new TransformationRegion(world);
          if (region.load(worldSection.getConfigurationSection(regionName), _plugin.getLogger()))
          {
            if (getWorldGuard().getRegionManager(world).getRegionExact(regionName) == null)
            {
              _plugin.getLogger().severe("The referenced transformation region " + regionName + " does not exist.");
            }

            // Allow the region to be set in the hope that it will be created
            // later.
            _plugin.getLogger().info("Loaded region " + region);
            addTransformatonRegion(region);
          }
          else
          {
            _plugin.getLogger().severe("Region " + regionName + " could not be loaded.");
          }
        }
      }
    } // for all referenced worlds
  } // loadTransformationRegions

  // --------------------------------------------------------------------------
  /**
   * Load the regions where use of /become is explicitly allowed or denied.
   * 
   * @param becomeSection the "regions.become" section of the configuration.
   */
  public void loadBecomeRegions(ConfigurationSection becomeSection)
  {
    for (String worldName : becomeSection.getKeys(false))
    {
      World world = Bukkit.getWorld(worldName);
      if (world == null)
      {
        _plugin.getLogger().severe("Invalid world name " + worldName +
                                   " referenced restricting /become.");
      }
      else
      {
        HashMap<String, Boolean> allowedMap = new HashMap<String, Boolean>();
        _becomeRegions.put(world, allowedMap);

        ConfigurationSection worldSection = becomeSection.getConfigurationSection(worldName);
        for (String regionName : worldSection.getKeys(false))
        {
          boolean allowed = worldSection.getBoolean(regionName);
          allowedMap.put(regionName, allowed);

          StringBuilder message = new StringBuilder();
          message.append("In world ");
          message.append(world.getName());
          message.append(", region ");
          message.append(regionName);
          message.append(", /become is ");
          message.append(allowed ? "allowed." : "not allowed.");
          _plugin.getLogger().info(message.toString());

          if (getWorldGuard().getRegionManager(world).getRegionExact(regionName) == null)
          {
            _plugin.getLogger().severe("The referenced /become region " + regionName + " does not exist.");
          }
        }
      } // valid world name
    } // for all worlds
  } // loadBecomeRegions

  // --------------------------------------------------------------------------
  /**
   * The owningn plugin.
   */
  protected JavaPlugin                                            _plugin;

  /**
   * Map from World to a map from region name to TransformationRegion instance.
   */
  protected HashMap<World, HashMap<String, TransformationRegion>> _transformationRegions = new HashMap<World, HashMap<String, TransformationRegion>>();

  /**
   * Map from World to a map from region name to boolean flag that is true if
   * /become is allowed.
   */
  protected HashMap<World, HashMap<String, Boolean>>              _becomeRegions         = new HashMap<World, HashMap<String, Boolean>>();

  /**
   * The WorldGuard plugin.
   */
  protected WorldGuardPlugin                                      _worldGuard;
} // class RegionTracker