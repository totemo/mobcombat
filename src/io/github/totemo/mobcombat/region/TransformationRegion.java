package io.github.totemo.mobcombat.region;

import java.util.logging.Logger;

import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

// ----------------------------------------------------------------------------
/**
 * A type of region that describes a forced transformation of a player.
 */
public class TransformationRegion
{
  /**
   * Constructor.
   * 
   * @param world the world in which this region exists.
   */
  public TransformationRegion(World world)
  {
    _world = world;
  }

  // --------------------------------------------------------------------------
  /**
   * Load the region from the specified section.
   * 
   * @param section the config section.
   * @param logger the logger.
   * @return true if the region was loaded without error.
   */
  public boolean load(ConfigurationSection section, Logger logger)
  {
    _name = section.getName();
    _enterMob = section.getString("enter");
    _leaveMob = section.getString("leave");
    if (getEnterMob() != null || getLeaveMob() != null)
    {
      return true;
    }
    logger.severe("region " + section.getCurrentPath() + " doesn't trigger transformation on entry or exit");
    return false;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the name of this region.
   * 
   * @return the name of this region.
   */
  public String getName()
  {
    return _name;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the world in which this region is valid.
   * 
   * @return the world in which this region is valid.
   */
  public World getWorld()
  {
    return _world;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the name of the type of mob to transform the player into on entry.
   * 
   * TODO: restructure to return MobType instead.
   * 
   * @return the name of the type of mob to transform the player into on entry.
   */
  public String getEnterMob()
  {
    return _enterMob;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the name of the type of mob to transform the player into on exit.
   * 
   * TODO: restructure to return MobType instead.
   * 
   * @return the name of the type of mob to transform the player into on exit.
   */
  public String getLeaveMob()
  {
    return _leaveMob;
  }

  // --------------------------------------------------------------------------
  /**
   * Format details for human consumption in the log.
   * 
   * @return the string representation of this region in the log.
   */
  @Override
  public String toString()
  {
    StringBuilder str = new StringBuilder();
    str.append(getName());
    str.append(" world: ");
    str.append(_world.getName());
    if (getEnterMob() != null)
    {
      str.append(" enter: ");
      str.append(getEnterMob());
    }
    if (getLeaveMob() != null)
    {
      str.append(" leave: ");
      str.append(getLeaveMob());
    }
    return str.toString();
  }

  // --------------------------------------------------------------------------
  /**
   * The name of the region.
   */
  protected String _name;

  /**
   * The world containing the region.
   */
  protected World  _world;

  /**
   * The name of the type of mob to transform the player into on entry.
   */
  protected String _enterMob;

  /**
   * The name of the type of mob to transform the player into on exit.
   */
  protected String _leaveMob;
} // class TransformationRegion