package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.mobs.MobType;
import io.github.totemo.mobcombat.util.InventoryHelper;
import io.github.totemo.mobcombat.util.Messages;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

// ----------------------------------------------------------------------------
/**
 * Tracks instances of {@link MobPlayer} corresponding to online Players and
 * manages their transformation into mobs.
 */
public class MobPlayerTracker implements Listener
{
  // --------------------------------------------------------------------------
  /**
   * Constructor.
   * 
   * @param plugin the owning plugin, for use of the logger, etc.
   */
  public MobPlayerTracker(MobCombat plugin)
  {
    _plugin = plugin;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the MobPlayer corresponding to the specified player.
   * 
   * This method should return non-null if the specified player is online.
   * 
   * @return the MobPlayer corresponding to the specified player.
   */
  public MobPlayer getMobPlayer(Player player)
  {
    MobPlayer mobPlayer = _onlinePlayers.get(player);

    // Saw a NullPointerException in loadMobPlayer() when an admin joined.
    // Possibly vanished admins cancel the join event? Work around:
    if (mobPlayer == null)
    {
      mobPlayer = loadMobPlayer(player);
    }
    return mobPlayer;
  }

  // --------------------------------------------------------------------------
  /**
   * Transform the corresponding player into the specified type of mob and show
   * special effects if the player is online.
   * 
   * @param mobPlayer the player.
   * @param newMobType the mob type; use MobType.HUMAN for human.
   * @param showEffects if true, special effects (e.g. lightning) are shown.
   */
  public void transformPlayer(MobPlayer mobPlayer, MobType newMobType, boolean showEffects)
  {
    // Reassert the disguise regardless of wether it has changed.
    _plugin.getMobDisguiseAPI().setDisguise(mobPlayer.getPlayer(), newMobType.getMob());

    MobType oldMobType = mobPlayer.getMobType();
    if (oldMobType != newMobType)
    {
      if (showEffects)
      {
        oldMobType.showTransformFromEffects(_plugin, mobPlayer);
      }
      oldMobType.onPlayerTransformFrom(mobPlayer);
      mobPlayer.setMobType(newMobType);
      if (showEffects)
      {
        newMobType.showTransformToEffects(_plugin, mobPlayer);
      }
      newMobType.onPlayerTransformTo(mobPlayer);

      // Show messages and apply characteristics.
      if (newMobType == MobType.HUMAN)
      {
        Messages.success(mobPlayer.getPlayer(), "You are human again.");
      }
      else
      {
        Messages.success(mobPlayer.getPlayer(), "You have been turned into " + newMobType.getDisplayName() + ".");
      }
      onBecomingMob(mobPlayer, true);
    }
  } // transformPlayer

  // --------------------------------------------------------------------------
  /**
   * Called when the plugin is enabled.
   * 
   * Load the {@link MobType} of all current online players and reapply mob
   * disguises.
   */
  public void onEnable()
  {
    for (Player player : Bukkit.getServer().getOnlinePlayers())
    {
      loadMobPlayer(player);
    }
    _plugin.getLogger().info("Tracking " + _onlinePlayers.size() + " online players.");
  }

  // --------------------------------------------------------------------------
  /**
   * Called when the plugin is disabled.
   * 
   * Save the {@link MobType} and related state of all online players and remove
   * mob disguises and cached state for the player.
   */
  public void onDisable()
  {
    _plugin.getLogger().info("Saving " + _onlinePlayers.size() + " online players.");
    for (Player player : Bukkit.getServer().getOnlinePlayers())
    {
      playerLeft(player);
    }
    _plugin.getLogger().info("Now tracking " + _onlinePlayers.size() + " online players.");
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerJoinEvent(PlayerJoinEvent event)
  {
    loadMobPlayer(event.getPlayer());
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerKickEvent(PlayerKickEvent event)
  {
    playerLeft(event.getPlayer());
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerQuitEvent(PlayerQuitEvent event)
  {
    playerLeft(event.getPlayer());
  }

  // --------------------------------------------------------------------------
  /**
   * This method is called after a player is loaded (on join or plugin reload)
   * to re-check that the quickbar does not contain any barred items and to show
   * the educational generic blurb about mob abilities.
   * 
   * @param mobPlayer the player.
   * @parqam giveTalisman if true, give the mob its talisman.
   */
  protected void onBecomingMob(MobPlayer mobPlayer, boolean giveTalisman)
  {
    Player player = mobPlayer.getPlayer();
    removeEndlessPotionBuffs(player);

    MobType mobType = mobPlayer.getMobType();
    if (mobType != MobType.HUMAN)
    {
      // Disguisecraft outputs the mob type message when turning into a
      // non-human mob. Additional information:
      for (String line : _plugin.getConfiguration().getMobTransformationMessage())
      {
        Messages.success(player, line);
      }

      if (giveTalisman)
      {
        giveTalisman(player, mobType);
      }
      addPotionBuffs(player, mobType);
      limitArmour(player, mobType);
      // Don't worry about sanitising the quickbar. We've had to block use
      // at the event level anyway.
    }

    // Generic capabilities modifications that apply to humans and mobs.
    setMotionCapabilities(player, mobType);

    // Save player state.
    savePlayer(player);
  } // onBecomingMob

  // --------------------------------------------------------------------------
  /**
   * Return the directory where per-player state information is stored.
   * 
   * @return the directory where per-player state information is stored.
   */
  protected File getMobPlayerDirectory()
  {
    return new File(_plugin.getDataFolder(), "players");
  }

  // --------------------------------------------------------------------------
  /**
   * Load the {@link MobType} state of a player, including the disguise, on
   * player join or plugin reload.
   * 
   * @param player the player.
   * @return the MobPplayer instance corresponding to player.
   */
  protected MobPlayer loadMobPlayer(Player player)
  {
    MobPlayer mobPlayer = new MobPlayer(player);
    _onlinePlayers.put(player, mobPlayer);
    try
    {
      // Only set up to transform into mobs if we can actually do it.
      if (_plugin.getMobDisguiseAPI().isAvailable())
      {
        mobPlayer.load(getMobPlayerDirectory(), _plugin);
      }

      // Don't show the message "You are human." Leave it silent.
      if (mobPlayer.getMobType() != MobType.HUMAN)
      {
        Messages.success(player, "You are " + mobPlayer.getMobType().getDisplayName() + ".");
      }
      onBecomingMob(mobPlayer, false);
    }
    catch (Exception ex)
    {
      _plugin.getLogger().severe(ex.getClass().getName() + " loading saved state of " + player.getName());
      ex.printStackTrace();
    }
    return mobPlayer;
  } // loadMobPlayer

  // --------------------------------------------------------------------------
  /**
   * Common implementation of PlayerKickEvent and PlayerQuitEvent handling.
   * 
   * @param player the player.
   */
  protected void playerLeft(Player player)
  {
    savePlayer(player);
    if (_plugin.getMobDisguiseAPI().isAvailable())
    {
      _plugin.getMobDisguiseAPI().setDisguise(player, MobType.HUMAN.getMob());
    }
    _onlinePlayers.remove(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Save the current MobPlayer's state.
   * 
   * @param player the player.
   */
  protected void savePlayer(Player player)
  {
    try
    {
      MobPlayer mobPlayer = getMobPlayer(player);
      mobPlayer.save(getMobPlayerDirectory());
    }
    catch (Exception ex)
    {
      _plugin.getLogger().severe(ex.getClass().getName() + " saving state of " + player.getName());
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Set the motion capabilities (fly or not, speed of walking and flying) of
   * the player to those of the {@link MobType}.
   * 
   * @param player the player.
   * @param mobType the mob type.
   */
  protected void setMotionCapabilities(Player player, MobType mobType)
  {
    player.setAllowFlight(player.getGameMode() == GameMode.CREATIVE || mobType.canFly());
    if (!player.getAllowFlight())
    {
      player.setFlying(false);
    }
    player.setFlySpeed(mobType.getFlySpeed());
    player.setWalkSpeed(mobType.getWalkSpeed());
  }

  // --------------------------------------------------------------------------
  /**
   * Give the player the talisman item of the specified {@link MobType}.
   * 
   * @param player the player.
   * @param mobType the mob type.
   */
  protected void giveTalisman(Player player, MobType mobType)
  {
    if (mobType.getTalisman() != null)
    {
      ItemStack talisman = new ItemStack(mobType.getTalisman());
      HashMap<Material, Integer> materialValues = _plugin.getConfiguration().getMaterialValues();
      if (InventoryHelper.forciblyAddItems(player, materialValues, false, talisman))
      {
        Messages.success(player, "Oops! You dropped something.");
      }
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Apply {@link MobType}-specific potion buffs to the player.
   * 
   * @param player the player.
   * @param mobType the mob type.
   */
  protected void addPotionBuffs(Player player, MobType mobType)
  {
    for (PotionEffect potion : mobType.getPotionBuffs())
    {
      player.addPotionEffect(potion, true);
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Remove high-duration potion buffs that were obtained by transformation into
   * a mob.
   * 
   * If the {@link MobType} has potion buffs defined, these will be applied
   * after this method is called.
   * 
   * @param player the player.
   */
  protected void removeEndlessPotionBuffs(Player player)
  {
    for (PotionEffect potion : player.getActivePotionEffects())
    {
      // Any potion effect longer than 10 minutes is assumed to from MobType.
      if (potion.getDuration() > 10 * 60 * 20)
      {
        player.removePotionEffect(potion.getType());
      }
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Remove unusable armour types (as defined by the MobType) from the armour
   * slots.
   * 
   * @param player the player.
   * @param mobType the mob type.
   */
  protected void limitArmour(Player player, MobType mobType)
  {
    // Move illegal armour items into illegalArmour.
    PlayerInventory inv = player.getInventory();
    ArrayList<ItemStack> illegalArmour = new ArrayList<ItemStack>();
    for (int slot = InventoryHelper.BOOTS_SLOT; slot <= InventoryHelper.HELMET_SLOT; ++slot)
    {
      // CTF allows non-armour items to be worn, so check isArmour().
      // That also includes a check for null.
      ItemStack armour = inv.getItem(slot);
      if (armour != null && !mobType.canWearArmour(armour))
      {
        illegalArmour.add(armour);
        inv.clear(slot);
      }
    }

    // Put them in normal inventory.
    if (illegalArmour.size() > 0)
    {
      HashMap<Material, Integer> materialValues = _plugin.getConfiguration().getMaterialValues();
      ItemStack[] movedArmour = illegalArmour.toArray(new ItemStack[illegalArmour.size()]);
      if (InventoryHelper.forciblyAddItems(player, materialValues, false, movedArmour))
      {
        Messages.success(player, "Oops! You dropped something.");
      }
    }
  } // limitArmour

  // --------------------------------------------------------------------------
  /**
   * Remove illegal tools and weapons from the player's quickbar.
   * 
   * @param player the player.
   * @param mobType the mob type.
   */
  protected void limitQuickbar(Player player, MobType mobType)
  {
    PlayerInventory inv = player.getInventory();
    ArrayList<ItemStack> illegal = new ArrayList<ItemStack>();
    for (int slot = InventoryHelper.QUICKBAR_MIN_SLOT; slot <= InventoryHelper.QUICKBAR_MAX_SLOT; ++slot)
    {
      ItemStack item = inv.getItem(slot);
      if (item != null)
      {
        if (!mobType.canUseSword(item) ||
            !mobType.canUseTool(item) ||
            (item.getType() == Material.BOW && !mobType.canUseBow()))
        {
          illegal.add(item);

          // Move an arbitrary item from normal storage inventory into the
          // quickbar so that it won't be refilled with the banned item later.
          int safeSlot = InventoryHelper.findNonToolOrWeaponSlot(inv);
          if (safeSlot < 0)
          {
            // Unable to fill this quickbar slot with something "safe". This
            // probably means an inventory full of stone swords or similar.
            inv.clear(slot);
          }
          else
          {
            // Move the safe item into the quickbar.
            inv.setItem(slot, inv.getItem(safeSlot));
            inv.clear(safeSlot);
          }
        } // is a sword tool or bow that can't be used
      } // non null item
    } // for all quickbar slots

    // Try to put the illegal items in storage slots. Drop whatever won't fit.
    Location loc = player.getLocation();
    boolean dropped = false;
    for (int i = 0; i < illegal.size(); ++i)
    {
      ItemStack illegalItem = illegal.get(i);
      int emptySlot = InventoryHelper.findHighestEmptySlot(inv, InventoryHelper.STORAGE_MIN_SLOT,
        InventoryHelper.STORAGE_MAX_SLOT);
      if (emptySlot < 0)
      {
        dropped = true;
        loc.getWorld().dropItem(loc, illegalItem);
      }
      else
      {
        inv.setItem(emptySlot, illegalItem);
      }
    }

    if (dropped)
    {
      Messages.success(player, "As " + mobType.getDisplayName() + ", some tools and weapons are impossible to grasp.");
      Messages.success(player, "They won't fit in your quickbar. Make some room elsewhere in your inventory.");
    }
  }// limitQuickBar

  // --------------------------------------------------------------------------
  /**
   * The owning plugin.
   */
  protected MobCombat                  _plugin;

  /**
   * Map from Player to MobPlayer instance. Players are removed from the map
   * when they disconnect for whatever reason.
   */
  protected HashMap<Player, MobPlayer> _onlinePlayers = new HashMap<Player, MobPlayer>();

} // class MobPlayerTracker
