package io.github.totemo.mobcombat.util;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

// ----------------------------------------------------------------------------
/**
 * Help reading some common configuration file structures.
 */
public class ConfigHelper
{
  /**
   * Constructor.
   * 
   * @param logger the logger for messages.
   */
  public ConfigHelper(Logger logger)
  {
    _logger = logger;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the child section of the specified parent at path.
   * 
   * Log a severe error if the child section is not present.
   * 
   * @return the sub-section of the specified parent at path.
   */
  public ConfigurationSection requireSection(ConfigurationSection parent, String path)
  {
    ConfigurationSection section = parent.getConfigurationSection(path);
    if (section == null)
    {
      _logger.severe("Missing configuration section: " + getFullPath(parent, path));
    }
    return section;
  }

  // --------------------------------------------------------------------------
  /**
   * Load a sound from path in the configuration section.
   * 
   * Log an error if the sound is misconfigured in the file (e.g. invalid name).
   * Log a warning if warnIfMissing is true and the configuration path is
   * absent.
   * 
   * @param parent the configuration section containing path.
   * @param path the path of the sound name in the section.
   * @param def the default sound to use if not specified or the configuration
   *          is erroneous.
   * @param warnIfMissing if true, log a warning if the path is missing from the
   *          configuration.
   */
  public Sound loadSound(ConfigurationSection parent, String path, Sound def, boolean warnIfMissing)
  {
    String soundName = parent.getString(path);
    if (soundName == null)
    {
      if (warnIfMissing)
      {
        String defName = (def == null) ? "none" : def.name();
        _logger.warning("Missing sound setting: " + getFullPath(parent, path) + "; defaulting to " + defName);
      }
      return def;
    }

    try
    {
      return Sound.valueOf(soundName.toUpperCase());
    }
    catch (Exception ex)
    {
      _logger.severe("Invalid sound specified for " + getFullPath(parent, path));
      return def;
    }
  } // loadSound

  // --------------------------------------------------------------------------
  /**
   * Load a material from path in the configuration section.
   * 
   * Log an error if the material is misconfigured in the file (e.g. invalid
   * name). Log a warning if warnIfMissing is true and the configuration path is
   * absent.
   * 
   * @param parent the configuration section containing path.
   * @param path the path of the material name in the section.
   * @param def the default material to use if not specified or the
   *          configuration is erroneous.
   * @param warnIfMissing if true, log a warning if the path is missing from the
   *          configuration.
   */
  public Material loadMaterial(ConfigurationSection parent, String path, Material def, boolean warnIfMissing)
  {
    String materialName = parent.getString(path);
    if (materialName == null)
    {
      if (warnIfMissing)
      {
        String defName = (def == null) ? "none" : def.name();
        _logger.warning("Missing material setting: " + getFullPath(parent, path) + "; defaulting to " + defName);
      }
      return def;
    }

    Material material = Material.matchMaterial(materialName);
    if (material == null)
    {
      _logger.severe("Invalid material specified for " + getFullPath(parent, path));
      return def;
    }
    return material;
  } // loadMaterial

  // --------------------------------------------------------------------------
  /**
   * Load the Set of Materials from the list of strings at path in the parent
   * section.
   * 
   * The materials can be listed as names or numbers. If the string list is
   * completely absent, log a sever error. If it is empty and warnIfEmpty is
   * true, issue a warning.
   * 
   * @param parent the configuration section containing path.
   * @param path the path of the string list of material names in the section.
   * @param warnIfEmpty if true, issue a warning if the string list is empty.
   * @return the Set of Materials.
   */
  public LinkedHashSet<Material> loadMaterials(ConfigurationSection parent, String path, boolean warnIfEmpty)
  {
    List<String> materialNames = parent.getStringList(path);
    if (materialNames == null)
    {
      _logger.severe("Missing list of materials: " + getFullPath(parent, path));
      return null;
    }

    LinkedHashSet<Material> materials = new LinkedHashSet<Material>();
    for (String materialName : materialNames)
    {
      Material material = Material.matchMaterial(materialName);
      if (material == null)
      {
        _logger.severe("invalid material specified for " + getFullPath(parent, path) + ": " + materialName);
      }
      else
      {
        materials.add(material);
      }
    }

    if (warnIfEmpty && materials.isEmpty())
    {
      _logger.warning("No materials specified in " + getFullPath(parent, path) + ".");
    }
    return materials;
  } // loadMaterials

  // --------------------------------------------------------------------------
  /**
   * Load the potion from the ConfigurationSection that is the child of parent
   * at the specified path.
   * 
   * The referenced section should take the form:
   * 
   * <pre>
   * fire_resistance:
   *   amplifier: 0
   *   duration: 2147483647
   *   ambient: false
   * </pre>
   * 
   * The section name must be a valid case-insensitive PotionEffectType value.
   * The example values above for amplifier, duration and ambient are the
   * defaults. Note that the amplifier value is one less than the conventional
   * potion strength number. For example, this encodes a Potion of Strength II:
   * 
   * <pre>
   * increase_damage:
   *   amplifier: 1
   * </pre>
   * 
   * You can omit all attributes for a particular potion using the following
   * YAML notation:
   * 
   * <pre>
   * fire_resistance: !!map {}
   * </pre>
   * 
   * @param parent the parent section containing the section named after the
   *          PotionEffectType.
   * @param path the path to the section, from the parent.
   * @return a PotionEffect; on error, log a severe error message and return
   *         null.
   */
  public PotionEffect loadPotion(ConfigurationSection parent, String path)
  {
    ConfigurationSection potionSection = requireSection(parent, path);
    if (potionSection == null)
    {
      return null;
    }

    PotionEffectType type = PotionEffectType.getByName(potionSection.getName());
    if (type == null)
    {
      _logger.severe(potionSection.getName() + " is not a valid potion type in " + getFullPath(parent, path) + ".");
      return null;
    }

    int duration = Math.max(0, potionSection.getInt("duration", Integer.MAX_VALUE));
    int amplifier = Math.max(0, potionSection.getInt("amplifier", 0));
    boolean ambient = potionSection.getBoolean("ambient", false);
    return new PotionEffect(type, duration, amplifier, ambient);
  } // loadPotion

  // --------------------------------------------------------------------------
  /**
   * Load all of the potions under the configuration section that is the child
   * of parent specified by path.
   * 
   * Return the non-null empty set if that section is missing or empty.
   * 
   * @param parent the parent section containing the section specified by path.
   * @param path the path relative to parent of the configuration section
   *          containing multiple potions in {@link #loadPotion()} format.
   * @param warnIfEmpty if true, log a warning if the returned set of potions is
   *          empty.
   * @return a non-null set of PotionEffect instances.
   */
  public HashSet<PotionEffect> loadPotions(ConfigurationSection parent, String path, boolean warnIfEmpty)
  {
    HashSet<PotionEffect> potions = new HashSet<PotionEffect>();
    ConfigurationSection potionsSection = requireSection(parent, path);
    if (potionsSection != null)
    {
      for (String potionPath : potionsSection.getKeys(false))
      {
        PotionEffect potion = loadPotion(potionsSection, potionPath);
        if (potion != null)
        {
          potions.add(potion);
        }
      }
    }
    if (warnIfEmpty && potions.size() == 0)
    {
      _logger.warning("No potions specified in " + getFullPath(parent, path) + ".");
    }
    return potions;
  } // loadPotions

  // --------------------------------------------------------------------------
  /**
   * Format the parent section and the path relative to it into a full path from
   * the root.
   * 
   * @param parent the parent section.
   * @param path the path within that section.
   * @return the full absolute path.
   */
  public static String getFullPath(ConfigurationSection parent, String path)
  {
    return (parent == parent.getRoot()) ? path : parent.getCurrentPath() + "." + path;
  }

  // --------------------------------------------------------------------------
  /**
   * The logger.
   */
  protected Logger _logger;
} // class ConfigHelper