package io.github.totemo.mobcombat.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

// ----------------------------------------------------------------------------
/**
 * Defines uniform formatting conventions for messages sent to players.
 */
public class Messages
{
  /**
   * The primary colour of successful message output.
   */
  public static ChatColor PRIMARY_COLOUR   = ChatColor.GOLD;

  /**
   * The secondary colour of successful message output.
   */
  public static ChatColor SECONDARY_COLOUR = ChatColor.YELLOW;

  /**
   * The colour of separators.
   */
  public static ChatColor SEPARATOR_COLOUR = ChatColor.GOLD;

  /**
   * The colour of messages indicating failure.
   */
  public static ChatColor FAILURE_COLOUR   = ChatColor.DARK_RED;

  // --------------------------------------------------------------------------
  /**
   * send the recipient a message indicating some successful action.
   * 
   * @param recipient the recipient of the message.
   * @param message the message.
   */
  public static void success(CommandSender recipient, String message)
  {
    StringBuilder text = new StringBuilder();
    text.append(PRIMARY_COLOUR);
    text.append(message);
    recipient.sendMessage(text.toString());
  }

  // --------------------------------------------------------------------------
  /**
   * send the recipient a message showing the specified descriptive label
   * followed by the specified value.
   * 
   * @param recipient the recipient of the message.
   * @param label the label before the separator.
   * @param separator the separator.
   * @param value the value.
   */
  public static void value(CommandSender recipient, String label, String separator, String value)
  {
    StringBuilder text = new StringBuilder();
    text.append(PRIMARY_COLOUR);
    text.append(label);
    text.append(SEPARATOR_COLOUR);
    text.append(separator);
    text.append(SECONDARY_COLOUR);
    text.append(value);
    recipient.sendMessage(text.toString());
  }

  // --------------------------------------------------------------------------
  /**
   * send the recipient a message indicating a failure.
   * 
   * @param recipient the recipient of the message.
   * @param message the message.
   */
  public static void failure(CommandSender recipient, String message)
  {
    StringBuilder text = new StringBuilder();
    text.append(FAILURE_COLOUR);
    text.append(message);
    recipient.sendMessage(text.toString());
  }

} // class Messages