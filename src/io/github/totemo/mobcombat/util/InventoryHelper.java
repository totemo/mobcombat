package io.github.totemo.mobcombat.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

// --------------------------------------------------------------------------
/**
 * Helper for managing player inventories.
 */
public class InventoryHelper
{
  /**
   * Index of the leftmost slot in the quickbar.
   */
  public static final int               QUICKBAR_MIN_SLOT = 0;

  /**
   * Index of the rightmost slot in the quickbar.
   */
  public static final int               QUICKBAR_MAX_SLOT = 8;

  /**
   * Index of the top left slot in the storage part of the inventory.
   */
  public static final int               STORAGE_MIN_SLOT  = 9;

  /**
   * Index of the bottom right slot in the storage part of the inventory.
   */
  public static final int               STORAGE_MAX_SLOT  = 35;

  /**
   * Index of boots slot. This is the minimum armour slot index.
   */
  public static final int               BOOTS_SLOT        = 36;

  /**
   * Index of leggings slot.
   */
  public static final int               LEGGINGS_SLOT     = 37;

  /**
   * Index of chestplate slot.
   */
  public static final int               CHESTPLATE_SLOT   = 38;

  /**
   * Index of helmet slot. This is the maximum armour slot index.
   */
  public static final int               HELMET_SLOT       = 39;

  /**
   * The set of all vanilla armour types.
   */
  public static final HashSet<Material> ARMOUR            = new HashSet<Material>();
  static
  {
    // Not accounting for plugins that add new armour.
    ARMOUR.add(Material.LEATHER_HELMET);
    ARMOUR.add(Material.LEATHER_CHESTPLATE);
    ARMOUR.add(Material.LEATHER_LEGGINGS);
    ARMOUR.add(Material.LEATHER_BOOTS);
    ARMOUR.add(Material.GOLD_HELMET);
    ARMOUR.add(Material.GOLD_CHESTPLATE);
    ARMOUR.add(Material.GOLD_LEGGINGS);
    ARMOUR.add(Material.GOLD_BOOTS);
    ARMOUR.add(Material.CHAINMAIL_HELMET);
    ARMOUR.add(Material.CHAINMAIL_CHESTPLATE);
    ARMOUR.add(Material.CHAINMAIL_LEGGINGS);
    ARMOUR.add(Material.CHAINMAIL_BOOTS);
    ARMOUR.add(Material.IRON_HELMET);
    ARMOUR.add(Material.IRON_CHESTPLATE);
    ARMOUR.add(Material.IRON_LEGGINGS);
    ARMOUR.add(Material.IRON_BOOTS);
    ARMOUR.add(Material.DIAMOND_HELMET);
    ARMOUR.add(Material.DIAMOND_CHESTPLATE);
    ARMOUR.add(Material.DIAMOND_LEGGINGS);
    ARMOUR.add(Material.DIAMOND_BOOTS);
  };

  /**
   * The set of all vanilla sword types.
   */
  public static final HashSet<Material> SWORD             = new HashSet<Material>();
  static
  {
    SWORD.add(Material.WOOD_SWORD);
    SWORD.add(Material.STONE_SWORD);
    SWORD.add(Material.GOLD_SWORD);
    SWORD.add(Material.IRON_SWORD);
    SWORD.add(Material.DIAMOND_SWORD);
  };

  /**
   * The set of all vanilla weapon types (all swords plus bow).
   */
  public static final HashSet<Material> WEAPON            = new HashSet<Material>();
  static
  {
    WEAPON.addAll(SWORD);
    WEAPON.add(Material.BOW);
  };

  /**
   * The set of all vanilla tool types.
   */
  public static final HashSet<Material> TOOL              = new HashSet<Material>();
  static
  {
    TOOL.add(Material.WOOD_SPADE);
    TOOL.add(Material.WOOD_HOE);
    TOOL.add(Material.WOOD_AXE);
    TOOL.add(Material.WOOD_PICKAXE);
    TOOL.add(Material.STONE_SPADE);
    TOOL.add(Material.STONE_HOE);
    TOOL.add(Material.STONE_AXE);
    TOOL.add(Material.STONE_PICKAXE);
    TOOL.add(Material.GOLD_SPADE);
    TOOL.add(Material.GOLD_HOE);
    TOOL.add(Material.GOLD_AXE);
    TOOL.add(Material.GOLD_PICKAXE);
    TOOL.add(Material.IRON_SPADE);
    TOOL.add(Material.IRON_HOE);
    TOOL.add(Material.IRON_AXE);
    TOOL.add(Material.IRON_PICKAXE);
    TOOL.add(Material.DIAMOND_SPADE);
    TOOL.add(Material.DIAMOND_HOE);
    TOOL.add(Material.DIAMOND_AXE);
    TOOL.add(Material.DIAMOND_PICKAXE);
  };

  // --------------------------------------------------------------------------
  /**
   * Return true if the specified ItemStack is armour.
   * 
   * @param item the ItemStack; can be null.
   * @return true if the specified ItemStack is armour.
   */
  public static boolean isArmour(ItemStack item)
  {
    return (item == null) ? false : ARMOUR.contains(item.getType());
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if the specified ItemStack is a weapon.
   * 
   * @param item the ItemStack; can be null.
   * @return true if the specified ItemStack is a weapon.
   */
  public static boolean isWeapon(ItemStack item)
  {
    return (item == null) ? false : WEAPON.contains(item.getType());
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if the specified ItemStack is a tool.
   * 
   * @param item the ItemStack; can be null.
   * @return true if the specified ItemStack is a tool.
   */
  public static boolean isTool(ItemStack item)
  {
    return (item == null) ? false : TOOL.contains(item.getType());
  }

  // --------------------------------------------------------------------------
  /**
   * Return the lowest slot index in the normal storage inventory (excludes the
   * quickbar) that is not empty and contains an item that is NOT a tool or a
   * weapon.
   * 
   * @param inv the inventory.
   * @return the index of the slot or -1 if no such slot exists.
   */
  public static int findNonToolOrWeaponSlot(Inventory inv)
  {
    for (int slot = STORAGE_MIN_SLOT; slot <= STORAGE_MAX_SLOT; ++slot)
    {
      ItemStack item = inv.getItem(slot);
      if (item != null && !isTool(item) && !isWeapon(item))
      {
        return slot;
      }
    }
    return -1;
  }

  // --------------------------------------------------------------------------
  /**
   * Add the specified items to the player's inventory, dropping cheap items on
   * the ground if necessary to make room.
   * 
   * @param player the player.
   * @param materialValues a map from Material to the corresponding value
   *          ordinal of the material. Lower values are cheaper.
   * @param dropNaturally if true, items removed to make room are dropped
   *          naturally, with an offset; otherwise they land at the player
   *          location.
   * @param items the items to add.
   * @return true if items were dropped.
   */
  public static boolean forciblyAddItems(Player player, HashMap<Material, Integer> materialValues,
                                         boolean dropNaturally, ItemStack... items)
  {
    Inventory inv = player.getInventory();
    HashMap<Integer, ItemStack> spareItems = inv.addItem(items);
    if (spareItems.size() > 0)
    {
      Location loc = player.getLocation();
      int[] preferredSlots = rankInventorySlots(inv, STORAGE_MIN_SLOT, STORAGE_MAX_SLOT, materialValues);
      int index = 0;
      for (ItemStack spareItem : spareItems.values())
      {
        // Remove the next cheapest item to make room for the spare item.
        ItemStack droppedItem = inv.getItem(preferredSlots[index]);
        inv.setItem(preferredSlots[index], spareItem);
        ++index;

        // Drop the cheap stuff on the ground.
        if (dropNaturally)
        {
          loc.getWorld().dropItemNaturally(loc, droppedItem);
        }
        else
        {
          loc.getWorld().dropItem(loc, droppedItem);
        }
      }
      return true;
    } // if items won't fit
    return false;
  } // forciblyAddItems

  // --------------------------------------------------------------------------
  /**
   * Give the player the specified item in their quickbar.
   * 
   * If all slots in the quickbar are full, move the "cheapest" item into normal
   * storage inventory space. If that area is also full, drop the cheapest item
   * from storage onto the ground before changing the quickbar in any way.
   * 
   * If the player's inventory is full of materials that are not in the
   * specified set of cheap materials, consider the cheapest item to be that
   * with the lowest material ID number,
   * 
   * @param player the player.
   * @param item the item to give.
   * @param materialValues a map from Material to the corresponding value
   *          ordinal of the material. Lower values are cheaper.
   * @param dropNaturally if true, items removed to make room are dropped
   *          naturally, with an offset; otherwise they land at the player
   *          location.
   * @return a reference to the already-dropped item stack.
   */
  public static ItemStack putInQuickbar(Player player, ItemStack item, HashMap<Material, Integer> materialValues,
                                        boolean dropNaturally)
  {
    Inventory inv = player.getInventory();
    int emptySlot = findHighestEmptySlot(inv, QUICKBAR_MIN_SLOT, QUICKBAR_MAX_SLOT);
    if (emptySlot >= 0)
    {
      inv.setItem(emptySlot, item);
      return null;
    }

    // There was no room in the quickbar. Grab the rightmost quickbar slot.
    ItemStack quickbarItem = inv.getItem(QUICKBAR_MAX_SLOT);

    // Move the rightmost quickbar item into an empty storage slot.
    emptySlot = findHighestEmptySlot(inv, STORAGE_MIN_SLOT, STORAGE_MAX_SLOT);
    if (emptySlot >= 0)
    {
      inv.setItem(emptySlot, quickbarItem);
      inv.setItem(QUICKBAR_MAX_SLOT, item);
      return null;
    }

    // So no room in the storage. Find the cheapest storage slot and drop it.
    int[] slotIndices = rankInventorySlots(inv, STORAGE_MIN_SLOT, STORAGE_MAX_SLOT, materialValues);
    ItemStack storageItem = inv.getItem(slotIndices[0]);
    inv.setItem(slotIndices[0], quickbarItem);
    inv.setItem(QUICKBAR_MAX_SLOT, item);

    // Try to stack the removed item(s) with something already in the inventory.
    HashMap<Integer, ItemStack> spareItems = inv.addItem(storageItem);
    if (spareItems.size() == 1)
    {
      ItemStack droppedItem = spareItems.get(0);
      Location loc = player.getLocation();
      if (dropNaturally)
      {
        loc.getWorld().dropItemNaturally(loc, droppedItem);
      }
      else
      {
        loc.getWorld().dropItem(loc, droppedItem);
      }
      return droppedItem;
    }
    return null;
  } // putInQuickbar

  // --------------------------------------------------------------------------
  /**
   * Return the index of the highest numbered empty slot in the inventory in the
   * range [minSlot,maxSlot].
   * 
   * The highest numbered slot is chosen because English-speaking players tend
   * to populate their quickbar from left to right (IMO).
   * 
   * @param inv the Inventory.
   * @param minSlot the minimum slot number to check.
   * @param maxSlot the maximum slot number to check.
   * @return the index of the empty slot, or -1 if all slots are occupied.
   */
  public static int findHighestEmptySlot(Inventory inv, int minSlot, int maxSlot)
  {
    for (int i = maxSlot; i >= minSlot; --i)
    {
      if (inv.getItem(i) == null)
      {
        return i;
      }
    }
    return -1;
  }

  // --------------------------------------------------------------------------
  /**
   * Return an array containing the indices of all inventory slots in a
   * specified range in ascending order according to the value of the items they
   * contain.
   * 
   * The purpose of this is to facilitate dropping cheap items out of a player's
   * inventory. Since it is possible that those items could fall into lava and
   * be lost we'd like to drop the cheapest items possible.
   * 
   * For a slot containing N items of material M:
   * <ul>
   * <li>If it's empty, its value is 0.</li>
   * <li>If it contains a material in the materialValues HashMap, its value will
   * be 1024 * materialValues.get(M) + N.</li>
   * <li>For materials not in the materialValues HashMap, the value will be 1024
   * * (materialValues.size() + M.getId()) + N.</li>
   * </ul>
   * 
   * The ranking of materials that don't appear in the materialValues HashMap is
   * based on the observation that lower numbered materials tend to be cheaper
   * than higher numbered ones. This is far from true in all cases, so if you
   * want perfect results, fill in materialValues for everything.
   * 
   * No attempt is made to assign a value to enchantments.
   * 
   * @param inv the Inventory.
   * @param minSlot the minimum slot number to check.
   * @param maxSlot the maximum slot number to check.
   * @param materialValues a map from Material to the corresponding value
   *          ordinal of the material. The cheapest entry should have the value
   *          1, and the most expensive should be materialValues.size().
   * @return an array of (maxSlot-minSlot+1) slot indices in ascending order by
   *         value of their contents. That is, the first element of the array
   *         will be the index of the cheapest slot.
   */
  public static int[] rankInventorySlots(Inventory inv, int minSlot, int maxSlot, HashMap<Material, Integer> materialValues)
  {
    /**
     * Store the value of each slot.
     */
    class Slot
    {
      /**
       * Index of the slot in the range [minValue,maxValue].
       */
      public int index;

      /**
       * Material value from materialValues parameter. If not present, use
       * (materialValues.size() + material.getId()).
       */
      public int value;
    }

    // Compute the material values of all inventory slots in the range.
    Slot[] slots = new Slot[maxSlot - minSlot + 1];
    for (int i = 0; i < slots.length; ++i)
    {
      slots[i] = new Slot();
      slots[i].index = minSlot + i;

      ItemStack item = inv.getItem(i);
      if (item == null)
      {
        slots[i].value = 0;
      }
      else
      {
        Integer definedValue = materialValues.get(item.getType());
        if (definedValue == null)
        {
          slots[i].value = 1024 * (materialValues.size() + item.getTypeId()) + item.getAmount();
        }
        else
        {
          slots[i].value = 1024 * definedValue + item.getAmount();
        }
      }
    } // for all inventory slots

    // Sort slots in ascending order by value.
    Comparator<Slot> slotComparator = new Comparator<Slot>()
    {
      @Override
      public int compare(Slot a, Slot b)
      {
        return a.value - b.value;
      }
    };
    Arrays.sort(slots, slotComparator);

    // Move indices into result array.
    int[] slotIndices = new int[maxSlot - minSlot + 1];
    for (int i = 0; i < slotIndices.length; ++i)
    {
      slotIndices[i] = slots[i].index;
    }
    return slotIndices;
  } // rankInventorySlots

} // class InventoryHelper