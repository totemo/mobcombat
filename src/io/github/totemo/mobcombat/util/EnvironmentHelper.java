package io.github.totemo.mobcombat.util;

import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;

// --------------------------------------------------------------------------
/**
 * Utility class gathering methods that query environmental factors.
 */
public class EnvironmentHelper
{
  /**
   * Set of all Biomes where precipitation falls as rain, rather than snow.
   */
  public static final HashSet<Biome> RAINY_BIOMES = new HashSet<Biome>();
  static
  {
    // Configure the set of Biomes with liquid precipitation.
    RAINY_BIOMES.add(Biome.BEACH);
    RAINY_BIOMES.add(Biome.EXTREME_HILLS);
    RAINY_BIOMES.add(Biome.FOREST);
    RAINY_BIOMES.add(Biome.FOREST_HILLS);
    RAINY_BIOMES.add(Biome.JUNGLE);
    RAINY_BIOMES.add(Biome.JUNGLE_HILLS);
    RAINY_BIOMES.add(Biome.MUSHROOM_ISLAND);
    RAINY_BIOMES.add(Biome.MUSHROOM_SHORE);
    RAINY_BIOMES.add(Biome.OCEAN);
    RAINY_BIOMES.add(Biome.PLAINS);
    RAINY_BIOMES.add(Biome.RIVER);
    RAINY_BIOMES.add(Biome.SMALL_MOUNTAINS);
    RAINY_BIOMES.add(Biome.SWAMPLAND);
  }

  /**
   * Relative time of the start of daylight.
   */
  public static final int            DAY_START    = 0;

  /**
   * Relative time of the start of night.
   */
  public static final int            NIGHT_START  = 13000;

  // --------------------------------------------------------------------------
  /**
   * Return true if the world has a sun (only the overworld) and it is day time.
   * 
   * @param world the world.
   * @return true if the world has a sun (only the overworld) and it is day
   *         time.
   */
  public static boolean isDay(World world)
  {
    return world.getEnvironment() == Environment.NORMAL &&
           world.getTime() >= DAY_START && world.getTime() <= NIGHT_START;
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if it is raining at the specified location and there is no
   * shelter for the player.
   * 
   * @param loc the Location.
   * @return true if rain is falling in liquid form and there is no shelter
   *         above the location.
   */
  public static boolean isRaining(Location loc)
  {
    return loc.getWorld().hasStorm() &&
           loc.getBlock().getLightFromSky() == 15 &&
           isOpenToTheSky(loc) &&
           RAINY_BIOMES.contains(loc.getBlock().getBiome());
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if the blocks above the specified location are all air.
   * 
   * The light level being 15 at loc is an assumed precondition of calling this
   * method. Unfortunately, that doesn't detect a glass roof above the player.
   * 
   * Bukkit API World.getHighestBlockYAt(Location) considers glass to be air,
   * contrary to its own documentation, and is therefore useless.
   * 
   * @param loc the location.
   * @return the highest Y coordinate of a non-air block at the specified
   *         location.
   */
  public static boolean isOpenToTheSky(Location loc)
  {
    World world = loc.getWorld();
    int locX = loc.getBlockX();
    int locY = loc.getBlockY();
    int locZ = loc.getBlockZ();
    for (int y = locY + 1; y < world.getMaxHeight(); ++y)
    {
      Block block = world.getBlockAt(locX, y, locZ);
      if (block.getTypeId() != 0)
      {
        return false;
      }
    }
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if there is water at any of the totalHeight blocks that include
   * feetLoc and the (totalHeight-1) blocks above.
   * 
   * @param feetLoc the location of a player's feet.
   * @param totalHeight the total height in blocks of the the space to check for
   *          water.
   * @return true if there is water at any of the totalHeight blocks that
   *         include feetLoc and the (totalHeight-1) blocks above.
   */
  public static boolean waterExists(Location feetLoc, int totalHeight)
  {
    for (int i = 0; i < totalHeight; ++i)
    {
      Location loc = feetLoc.clone();
      loc.add(0, i, 0);
      Material material = loc.getBlock().getType();
      if (material == Material.WATER || material == Material.STATIONARY_WATER)
      {
        return true;
      }
    }
    return false;
  }

} // class EnvironmentHelper