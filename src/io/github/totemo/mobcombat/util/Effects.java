package io.github.totemo.mobcombat.util;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

// ----------------------------------------------------------------------------
/**
 * Schedules special effects.
 */
public class Effects
{
  // --------------------------------------------------------------------------
  /**
   * Schedule a damageless lightning strike at the specified location after a
   * certain number of ticks have elapsed.
   * 
   * @param plugin the owning plugin.
   * @param loc the location of the strike.
   * @param delay the delay in ticks before the effect.
   */
  public static void strikeLightningEffect(Plugin plugin, final Location loc, long delay)
  {
    if (delay >= 0)
    {
      Runnable task = new Runnable()
      {
        @Override
        public void run()
        {
          loc.getWorld().strikeLightningEffect(loc);
        }
      };
      Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, task, delay);
    }
  } // stikeLightningEffect

  // --------------------------------------------------------------------------
  /**
   * Draw a horizontal ring of (particle) effects around the specified centre.
   * 
   * @param plugin the owning plugin.
   * @param effect the particle effect to use.
   * @param centre the centre of the circle.
   * @param radius the radius of the circle.
   * @param points the number of points around the circle to draw particles.
   * @param delay the delay in ticks before drawing.
   */
  public static void particleRing(Plugin plugin, final Effect effect, final Location centre, final double radius,
                                  final int points, long delay)
  {
    if (delay >= 0)
    {
      Runnable task = new Runnable()
      {
        @Override
        public void run()
        {
          for (int i = 0; i < points; ++i)
          {
            Location loc = centre.clone();
            double angle = i * 2 * Math.PI / points;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            loc.add(x, 0, z);
            // Does data do anything here? Particle count?
            centre.getWorld().playEffect(loc, effect, 10);
          }
        }
      };
      Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, task, delay);
    }
  } // particleRing

  // --------------------------------------------------------------------------
  /**
   * Animate an expanding ring of fire and smoke.
   * 
   * Near the centre it is fire, and smoke past half the radius.
   * 
   * @param plugin the owning plugin.
   * @param centre the centre of the ring.
   * @param maxRadius the maximum radius achieved by the end of the animation.
   * @param duration the total duration of the animation in ticks.
   */
  public static void expandingFireRing(Plugin plugin, final Location centre, double maxRadius, int duration)
  {
    if (duration >= 0)
    {
      Location smokeCentre = centre.clone();
      smokeCentre.add(0, 1.0, 0);

      final double RADIUS_STEP = 0.75;
      int steps = (int) Math.round(maxRadius / RADIUS_STEP);
      for (int i = 0; i < steps; ++i)
      {
        boolean smoke = (i > steps / 2);
        Effect effect = smoke ? Effect.SMOKE : Effect.MOBSPAWNER_FLAMES;
        int delay = (int) Math.round(i * duration / (double) steps);
        int points = 4 + i * (smoke ? 6 : 2);
        double radius = RADIUS_STEP * (1 + i);
        Effects.particleRing(plugin, effect, smoke ? smokeCentre : centre, radius, points, delay);
      }
    }
  }
} // class Effects
