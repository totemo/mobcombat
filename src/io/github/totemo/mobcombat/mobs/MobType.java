package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import java.util.HashSet;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;

// ----------------------------------------------------------------------------
/**
 * Describes a type of mob that a player can turn into.
 */
public interface MobType
{
  // --------------------------------------------------------------------------
  /**
   * The MobType instance used to describe players that are not currently mobs.
   */
  public static final MobType HUMAN = new MobTypeHuman();

  // --------------------------------------------------------------------------
  /**
   * Return the programmatic name of this mob type.
   * 
   * @return the programmatic name of this mob type, in general the same as the
   *         EntityType value.
   */
  public String getName();

  // --------------------------------------------------------------------------
  /**
   * Return the display name of this mob type.
   * 
   * @return the display name of this mob type, for example "a spider" or
   *         "an enderman".
   */
  public String getDisplayName();

  // --------------------------------------------------------------------------
  /**
   * Return the mob type name passed to DisguiseCraft.
   * 
   * @return the mob type name passed to DisguiseCraft.
   */
  public String getMob();

  // --------------------------------------------------------------------------
  /**
   * Return the weight given to this mob when randomly selecting one from all
   * known types.
   * 
   * The probability of choosing a particular MobType is its probability weight
   * divided by the sum of the probability weights of all enabled MobTypes.
   * 
   * @return the weight given to this mob when randomly selecting one from all
   *         known types.
   */
  public float getProbabilityWeight();

  // --------------------------------------------------------------------------
  /**
   * Return true if the Player has permission to become this MobType using
   * /become.
   * 
   * @param player the Player.
   * @return true if the Player has permission to become this MobType using
   *         /become.
   */
  public boolean canPlayerBecome(Player player);

  // --------------------------------------------------------------------------
  /**
   * Return the full power level at which power regeneration ceases.
   * 
   * @return the full power level at which power regeneration ceases.
   */
  public float getFullPower();

  // --------------------------------------------------------------------------
  /**
   * Return the rate of power regeneration in power per second.
   * 
   * @return the rate of power regeneration in power per second.
   */
  public float getPowerRegenRate();

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can place the specified Material.
   * 
   * @param material the Material to place.
   * @return true if it can be placed.
   */
  public boolean canPlace(Material material);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can break the specified Material.
   * 
   * @param material the Material to break.
   * @return true if it can be broken.
   */
  public boolean canBreak(Material material);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can interact with the specified block, e.g. a
   * button, a chest.
   * 
   * @param material the material of the block.
   * @return true if it can be used.
   */
  public boolean canInteract(Material material);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can use the specified item.
   * 
   * @param material the material of the item.
   * @return true if it can be used.
   */
  public boolean canUseItem(Material material);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can use the specified sword.
   * 
   * @param itemInHand the item in the player's hand.
   * @return true if it can be used.
   */
  public boolean canUseSword(ItemStack itemInHand);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can use a bow.
   * 
   * @param itemInHand the item in the player's hand.
   * @return true if it can be used.
   */
  public boolean canUseBow();

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can use the specified item in its hand as a tool.
   * 
   * @param itemInHand the item in the player's hand.
   * @return true if it can be used.
   */
  public boolean canUseTool(ItemStack itemInHand);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can wear the specified armour.
   * 
   * @param armour the armour.
   * @return true if the mob can wear the specified armour.
   */
  public boolean canWearArmour(ItemStack armour);

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can fly.
   * 
   * @return true if the mob can fly.
   */
  public boolean canFly();

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob is hurt by water.
   * 
   * @return true if the mob is hurt by water.
   */
  public boolean isHurtByWater();

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob is hurt by direct sunlight.
   * 
   * @return true if the mob is hurt by direct sunlight.
   */
  public boolean isHurtBySun();

  // --------------------------------------------------------------------------
  /**
   * Return the walking speed as a Player.setWalkSpeed() value, in the range
   * [-1.0,+1.0].
   * 
   * @return the walking speed as a Player.setWalkSpeed() value, in the range
   *         [-1.0,+1.0].
   */
  public float getWalkSpeed();

  // --------------------------------------------------------------------------
  /**
   * Return the flying speed as a Player.setFlySpeed() value, in the range
   * [-1.0,+1.0].
   * 
   * @return the flying speed as a Player.setFlySpeed() value, in the range
   *         [-1.0,+1.0].
   */
  public float getFlySpeed();

  // --------------------------------------------------------------------------
  /**
   * Return the irregularly scheduled idle sound of the mob.
   * 
   * @return the irregularly scheduled idle sound of the mob.
   */
  public Sound getIdleSound();

  // --------------------------------------------------------------------------
  /**
   * Return the hurt sound of the mob.
   * 
   * @return the hurt sound of the mob.
   */
  public Sound getHurtSound();

  // --------------------------------------------------------------------------
  /**
   * Return the charge sound (battle cry) of the mob.
   * 
   * This is the sound player when the mob charges (rushes) at the player.
   * 
   * @return the charge sound (battle cry) of the mob.
   */
  public Sound getChargeSound();

  // --------------------------------------------------------------------------
  /**
   * Return the angry sound of the mob.
   * 
   * @return the angry sound of the mob.
   */
  public Sound getAngrySound();

  // --------------------------------------------------------------------------
  /**
   * Return the special attack sound of the mob.
   * 
   * @return the special attack sound of the mob.
   */
  public Sound getSpecialSound();

  // --------------------------------------------------------------------------
  /**
   * Return the death sound of the mob.
   * 
   * @return the death sound of the mob.
   */
  public Sound getDeathSound();

  // --------------------------------------------------------------------------
  /**
   * Return the volume of the mob's sound effects.
   * 
   * @return the volume of the mob's sound effects.
   */
  public float getSoundVolume();

  // --------------------------------------------------------------------------
  /**
   * Return the cool-down period, in ticks, that must elapse before the player
   * can voluntarily initiate another sound effect.
   * 
   * @return the cool-down period, in ticks, that must elapse before the player
   *         can voluntarily initiate another sound effect.
   */
  public int getSoundCoolDown();

  // --------------------------------------------------------------------------
  /**
   * Return the mob's power object, which activates the special ability on right
   * click.
   * 
   * @return the mob's power object.
   */
  public Material getTalisman();

  // --------------------------------------------------------------------------
  /**
   * Return the power cost of using the special power.
   * 
   * @return the power cost of using the special power.
   */
  public float getSpecialPowerCost();

  // --------------------------------------------------------------------------
  /**
   * Return the range of the special power.
   * 
   * @return the range of the special power.
   */
  public float getSpecialRange();

  // --------------------------------------------------------------------------
  /**
   * Return the effect multiplier (if relevant), e.g. explosive force, of the
   * special power.
   * 
   * @return the effect multiplier (if relevant), e.g. explosive force, of the
   *         special power.
   */
  public float getSpecialMultiplier();

  // --------------------------------------------------------------------------
  /**
   * Return the damage in hearts applied to mobs directly contacted
   * (right-click) by this mob's special attack. type.
   * 
   * @return the damage in hearts applied to mobs directly contacted
   *         (right-click) by this mob's special attack. type.
   */
  public double getSpecialDamage();

  // --------------------------------------------------------------------------
  /**
   * Return the set of damaging potion effects applied to mobs directly
   * contacted (right-click) by this mob's special attack. type.
   * 
   * @return the set of damaging potion effects applied to mobs directly
   *         contacted (right-click) by this mob's special attack. type.
   */
  public HashSet<PotionEffect> getSpecialPotionEffects();

  // --------------------------------------------------------------------------
  /**
   * Return the minimum number of ticks after the special ability is used before
   * it can be used again.
   * 
   * @return the minimum number of ticks after the special ability is used
   *         before it can be used again.
   */
  public int getSpecialCoolDown();

  // --------------------------------------------------------------------------
  /**
   * Return the set of potion buffs applied to this mob type.
   * 
   * @return the set of potion buffs applied to this mob type.
   */
  public HashSet<PotionEffect> getPotionBuffs();

  // --------------------------------------------------------------------------
  /**
   * Return the amount of damage in hearts to be applied per second when a mob
   * that is hurt by water is in contact with it.
   * 
   * This damage is in addition to that caused by any potion effects applied to
   * the mob by onWaterDamage().
   * 
   * @return the amount of damage in hearts to be applied per second when a mob
   *         that is hurt by water is in contact with it.
   */
  public double getWaterDamagePerSecond();

  // --------------------------------------------------------------------------
  /**
   * Return the set of damaging potion effects applied to mobs attacked by this
   * type.
   * 
   * @return the set of damaging potion effects applied to mobs attacked by this
   *         type.
   */
  public HashSet<PotionEffect> getAttackPotionEffects();

  // --------------------------------------------------------------------------
  /**
   * Apply a set of harmful effects when the mob is in contact with water.
   * 
   * @param mobPlayer the affected player.
   */
  public void onWaterDamage(MobPlayer mobPlayer);

  // --------------------------------------------------------------------------
  /**
   * Called when the mob activates its special ability.
   * 
   * @param mobPlayer the player.
   * @param livingEntity the living entity that was right clicked on (may be
   *          null for right clicks on air).
   */
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity livingEntity);

  // --------------------------------------------------------------------------
  /**
   * Handler called when the player is transformed into this mob type.
   * 
   * @param mobPlayer the player.
   */
  public void onPlayerTransformTo(MobPlayer mobPlayer);

  // --------------------------------------------------------------------------
  /**
   * Show special effects when transforming to this mob type.
   * 
   * @param plugin the owning plugin.
   * @param mobPlayer the player.
   */
  public void showTransformToEffects(Plugin plugin, MobPlayer mobPlayer);

  // --------------------------------------------------------------------------
  /**
   * Handler called when the player is transformed from this mob type to some
   * other.
   * 
   * @param mobPlayer the player.
   */
  public void onPlayerTransformFrom(MobPlayer mobPlayer);

  // --------------------------------------------------------------------------
  /**
   * Show special effects when transforming from this mob type to some other.
   * 
   * @param plugin the owning plugin.
   * @param mobPlayer the player.
   */
  public void showTransformFromEffects(Plugin plugin, MobPlayer mobPlayer);

  // --------------------------------------------------------------------------
  /**
   * Handler called when this mob dies.
   * 
   * @param mobPlayer the player.
   * @param event the event.
   */
  public void onPlayerDeath(MobPlayer mobPlayer, PlayerDeathEvent event);

  // --------------------------------------------------------------------------
  /**
   * Load the configuration for this MobType from the specified configuration
   * file section.
   * 
   * @param section the configuration file section.
   * @param logger used to log messages.
   */
  public void loadConfiguration(ConfigurationSection section, Logger logger);
} // class MobType
