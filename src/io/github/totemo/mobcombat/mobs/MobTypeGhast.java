package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

// --------------------------------------------------------------------------
/**
 * Custom implementation details for the Ghast mob type.
 */
public class MobTypeGhast extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();
    LargeFireball fireball = player.launchProjectile(LargeFireball.class);
    fireball.setYield(fireball.getYield() * mobType.getSpecialMultiplier());
    fireball.setIsIncendiary(true); // Doesn't appear to work. :/
    fireball.setShooter(player);
    mobPlayer.addPower(-mobType.getSpecialPowerCost());
    mobPlayer.playSoundInWorld(mobType.getSpecialSound());
  }
} // class MobTypeGhast