package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import java.util.HashSet;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;

// --------------------------------------------------------------------------
/**
 * Describes the capabilities of an ordinary player.
 */
public class MobTypeHuman implements MobType
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getName()
   */
  @Override
  public String getName()
  {
    return "Human";
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getDisplayName()
   */
  @Override
  public String getDisplayName()
  {
    return "human";
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getMob()
   */
  @Override
  public String getMob()
  {
    return "Human";
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getProbabilityWeight()
   * 
   *      Humans should never be selected randomly.
   */
  @Override
  public float getProbabilityWeight()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canPlayerBecome(org.bukkit.entity.Player)
   */
  @Override
  public boolean canPlayerBecome(Player player)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getFullPower()
   */
  @Override
  public float getFullPower()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getPowerRegenRate()
   */
  @Override
  public float getPowerRegenRate()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canPlace(org.bukkit.Material)
   */
  @Override
  public boolean canPlace(Material material)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canBreak(org.bukkit.Material)
   */
  @Override
  public boolean canBreak(Material material)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canInteract(org.bukkit.Material)
   */
  @Override
  public boolean canInteract(Material material)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseItem(org.bukkit.Material)
   */
  @Override
  public boolean canUseItem(Material material)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseSword(org.bukkit.inventory.ItemStack)
   */
  @Override
  public boolean canUseSword(ItemStack itemInHand)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseBow()
   */
  @Override
  public boolean canUseBow()
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseTool(org.bukkit.inventory.ItemStack)
   */
  @Override
  public boolean canUseTool(ItemStack itemInHand)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canWearArmour(org.bukkit.inventory.ItemStack)
   */
  @Override
  public boolean canWearArmour(ItemStack armour)
  {
    return true;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canFly()
   */
  @Override
  public boolean canFly()
  {
    return false;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#isHurtByWater()
   */
  @Override
  public boolean isHurtByWater()
  {
    return false;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#isHurtBySun()
   */
  @Override
  public boolean isHurtBySun()
  {
    return false;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getFlySpeed()
   */
  @Override
  public float getFlySpeed()
  {
    return 0.1f;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getWalkSpeed()
   */
  @Override
  public float getWalkSpeed()
  {
    return 0.2f;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getIdleSound()
   */
  @Override
  public Sound getIdleSound()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getHurtSound()
   */
  @Override
  public Sound getHurtSound()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getChargeSound()
   */
  @Override
  public Sound getChargeSound()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getAngrySound()
   */
  @Override
  public Sound getAngrySound()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialSound()
   */
  @Override
  public Sound getSpecialSound()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getDeathSound()
   */
  @Override
  public Sound getDeathSound()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSoundVolume()
   */
  @Override
  public float getSoundVolume()
  {
    return 3;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSoundCoolDown()
   */
  @Override
  public int getSoundCoolDown()
  {
    return 20;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getTalisman()
   */
  @Override
  public Material getTalisman()
  {
    return null;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialPowerCost()
   */
  @Override
  public float getSpecialPowerCost()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialRange()
   */
  @Override
  public float getSpecialRange()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialMultiplier()
   */
  @Override
  public float getSpecialMultiplier()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialDamage()
   */
  @Override
  public double getSpecialDamage()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialPotionEffects()
   */
  @Override
  public HashSet<PotionEffect> getSpecialPotionEffects()
  {
    return NO_POTION_EFFECTS;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialCoolDown()
   */
  @Override
  public int getSpecialCoolDown()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getPotionBuffs()
   */
  @Override
  public HashSet<PotionEffect> getPotionBuffs()
  {
    return NO_POTION_EFFECTS;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getWaterDamagePerSecond()
   */
  @Override
  public double getWaterDamagePerSecond()
  {
    return 0;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getAttackPotionEffects()
   */
  @Override
  public HashSet<PotionEffect> getAttackPotionEffects()
  {
    return NO_POTION_EFFECTS;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onWaterDamage(io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void onWaterDamage(MobPlayer mobPlayer)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onPlayerTransformTo(io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void onPlayerTransformTo(MobPlayer player)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#showTransformToEffects(org.bukkit.plugin.Plugin,
   *      io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void showTransformToEffects(Plugin plugin, MobPlayer mobPlayer)
  {
    // Empty.
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onPlayerTransformFrom(io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void onPlayerTransformFrom(MobPlayer player)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#showTransformFromEffects(org.bukkit.plugin.Plugin,
   *      io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void showTransformFromEffects(Plugin plugin, MobPlayer mobPlayer)
  {
    // Empty.
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onPlayerDeath(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.event.entity.PlayerDeathEvent)
   */
  @Override
  public void onPlayerDeath(MobPlayer player, PlayerDeathEvent event)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#loadConfiguration(org.bukkit.configuration.ConfigurationSection,
   *      java.util.logging.Logger)
   */
  @Override
  public void loadConfiguration(ConfigurationSection section, Logger logger)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * Empty set of potion effects.
   */
  protected static HashSet<PotionEffect> NO_POTION_EFFECTS = new HashSet<PotionEffect>();

} // class MobTypeHuman