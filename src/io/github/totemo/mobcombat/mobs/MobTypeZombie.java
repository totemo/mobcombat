package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

// ----------------------------------------------------------------------------
/**
 * Custom implementation details for the Zombie mob type.
 */
public class MobTypeZombie extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();
    if (living != null)
    {
      mobPlayer.addPower(-mobType.getSpecialPowerCost());
      mobPlayer.playSoundInWorld(mobType.getSpecialSound());
      living.damage(mobType.getSpecialDamage(), player);
      for (PotionEffect potion : mobType.getSpecialPotionEffects())
      {
        living.addPotionEffect(potion, true);
      }
    }
  }
} // class MobTypeZombie