package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.util.ConfigHelper;
import io.github.totemo.mobcombat.util.WeightedSelection;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

// ----------------------------------------------------------------------------
/**
 * Manages knowledge of all configured MobTypes.
 */
public class MobTypeRegistry
{
  // --------------------------------------------------------------------------
  /**
   * Set the known {@link MobType}s to be those defined in the specified
   * configuration section.
   * 
   * @param section the section of the plugin configuration file.
   */
  public void loadMobTypes(FileConfiguration configuration, Logger logger)
  {
    _mobTypes.clear();
    _mobTypes.put(MobType.HUMAN.getName().toLowerCase(), MobType.HUMAN);

    ConfigHelper config = new ConfigHelper(logger);
    ConfigurationSection mobs = config.requireSection(configuration, "mobs");
    if (mobs != null)
    {
      for (String mobTypeName : mobs.getKeys(false))
      {
        ConfigurationSection mobSection = mobs.getConfigurationSection(mobTypeName);

        boolean enabled = mobSection.getBoolean("enabled", true);
        if (enabled)
        {
          String mobTypeClassName = mobSection.getString("class", "MobTypeBase");
          try
          {
            Class<?> mobTypeClass = Class.forName(mobTypeClassName);
            MobType mobType = (MobType) mobTypeClass.newInstance();
            mobType.loadConfiguration(mobSection, logger);
            if (mobType.getName().equals(mobTypeName))
            {
              _mobTypes.put(mobTypeName.toLowerCase(), mobType);
              _choice.addChoice(mobType, mobType.getProbabilityWeight());
              logger.info("Loaded mob type: " + mobType.getName());
            }
            else
            {
              logger.severe("mob type " + mobTypeName + " has an invalid implementation");
            }
          }
          catch (Exception ex)
          {
            logger.severe(ex.getClass().getName() + ": invalid class for " + mobTypeName + ": " + mobTypeClassName);
          }
        }
      } // for
    }
  } // loadMobTypes

  // --------------------------------------------------------------------------
  /**
   * Return the set of all configured {@link MobType} names.
   * 
   * @return the set of all configured {@link MobType} types.
   */
  public Set<String> getAllMobTypes()
  {
    return _mobTypes.keySet();
  }

  // --------------------------------------------------------------------------
  /**
   * Return the {@link MobType} with the specified name, or null if not found.
   * 
   * @param name the case insensitive {@link MobType#getName()} value.
   * @return the {@link MobType} with the specified name, or null if not found.
   */
  public MobType getMobType(String name)
  {
    return _mobTypes.get(name.toLowerCase());
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if the player can become the specified mob type using /become.
   * 
   * Note that the player can be transformed into the mob type (if it exists) by
   * other means. This method is mainly concerned with the permission check on
   * the /become command.
   * 
   * @param mobTypeName the name of the mob type.
   * @return true if the player can become the specified mob type using /become.
   */
  public boolean canBecome(Player player, String mobTypeName)
  {
    MobType mobType = getMobType(mobTypeName);
    return mobType != null && mobType.canPlayerBecome(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Return a set of all mob types that the player has permission to transform
   * into using /become, listed in the same order as they were defined.
   * 
   * @return a set of all mob types that the player has permission to transform
   *         into using /become, listed in the same order as they were defined.
   */
  public LinkedHashSet<MobType> getAllowedTypes(Player player)
  {
    LinkedHashSet<MobType> types = new LinkedHashSet<MobType>();
    for (MobType mobType : _mobTypes.values())
    {
      if (mobType.canPlayerBecome(player))
      {
        types.add(mobType);
      }
    }
    return types;
  }

  // --------------------------------------------------------------------------
  /**
   * Choose a randomly selected mob type from the set of all known types, giving
   * equal probability to each possibility.
   * 
   * @return the randomly selected type.
   */
  public MobType chooseRandomMobType()
  {
    return _choice.choose();
  }

  // --------------------------------------------------------------------------
  /**
   * Map from lower-cased programmatic name to MobType instance in the order
   * defined by the configuration file.
   */
  protected LinkedHashMap<String, MobType> _mobTypes = new LinkedHashMap<String, MobType>();

  /**
   * Chooses a MobType (excluding MobType.HUMAN) according to their configured
   * probability weights.
   */
  protected WeightedSelection<MobType>     _choice   = new WeightedSelection<MobType>();
} // class MobTypeRegistry