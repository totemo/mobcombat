package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import java.util.HashSet;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

// ----------------------------------------------------------------------------
/**
 * Custom implementation details for the Enderman mob type.
 */
public class MobTypeEnderman extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();

    List<Block> blocks = player.getLastTwoTargetBlocks(TRANSPARENT_BLOCKS, (int) mobType.getSpecialRange());
    if (blocks.size() == 2)
    {
      Block secondLast = blocks.get(0);
      Block last = blocks.get(1);
      if (last.getType() != Material.AIR)
      {
        Location destination = secondLast.getLocation().clone();
        destination.add(0.5, 1, 0.5);
        destination.setPitch(player.getLocation().getPitch());
        destination.setYaw(player.getLocation().getYaw());

        // Is the destination in range?
        if (destination.distanceSquared(player.getLocation()) < mobType.getSpecialRange() * mobType.getSpecialRange())
        {
          mobPlayer.addPower(-mobType.getSpecialPowerCost());
          player.teleport(destination, TeleportCause.ENDER_PEARL);
          mobPlayer.playSoundInWorld(mobType.getSpecialSound());
        }
        else
        {
          // Frustrated.
          mobPlayer.playSoundInWorld(mobType.getAngrySound());
        }
      }
    }

    // Irrespective of whether the teleport succeeds or not, sprinkle some
    // particles on it.
    player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 0);
  } // onSpecialAbility

  // --------------------------------------------------------------------------
  /**
   * Set of block byte IDs that are considered transparent.
   */
  protected static final HashSet<Byte>     TRANSPARENT_BLOCKS = new HashSet<Byte>();

  /**
   * Set of block Materials that can be considered transparent despite being
   * obstacles.
   */
  protected static final HashSet<Material> PASSABLE_OBSTACLES = new HashSet<Material>();

  static
  {
    // Set up the blocks that are transparent to endermen.
    PASSABLE_OBSTACLES.add(Material.TRAP_DOOR);
    PASSABLE_OBSTACLES.add(Material.WOODEN_DOOR);
    PASSABLE_OBSTACLES.add(Material.IRON_DOOR_BLOCK);
    // Temporarily disable teleporting through glass for PvE event.
    // These should really be a configuration setting.
    // PASSABLE_OBSTACLES.add(Material.GLASS);
    // PASSABLE_OBSTACLES.add(Material.THIN_GLASS);
    PASSABLE_OBSTACLES.add(Material.COBBLE_WALL);
    PASSABLE_OBSTACLES.add(Material.FENCE);
    PASSABLE_OBSTACLES.add(Material.FENCE_GATE);
    PASSABLE_OBSTACLES.add(Material.IRON_FENCE);
    PASSABLE_OBSTACLES.add(Material.PORTAL);
    PASSABLE_OBSTACLES.add(Material.ENDER_PORTAL);
    for (Material material : Material.values())
    {
      if (material.isBlock() && (!material.isSolid() || PASSABLE_OBSTACLES.contains(material)))
      {
        TRANSPARENT_BLOCKS.add((byte) material.getId());
      }
    }
  } // static
} // class MobTypeEnderman