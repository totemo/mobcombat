package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;

// --------------------------------------------------------------------------
/**
 * Custom implementation details for the Wither mob type.
 */
public class MobTypeWither extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();
    WitherSkull projectile = player.launchProjectile(WitherSkull.class);
    projectile.setYield(projectile.getYield() * mobType.getSpecialMultiplier());
    projectile.setShooter(player);
    mobPlayer.addPower(-mobType.getSpecialPowerCost());
    mobPlayer.playSoundInWorld(mobType.getSpecialSound());
  }
} // class MobTypeWithersound