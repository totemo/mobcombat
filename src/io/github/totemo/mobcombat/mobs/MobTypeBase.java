package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;
import io.github.totemo.mobcombat.util.ConfigHelper;
import io.github.totemo.mobcombat.util.Effects;
import io.github.totemo.mobcombat.util.InventoryHelper;

import java.util.HashSet;
import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;

// --------------------------------------------------------------------------
/**
 * Abstract base class of {@link MobType} implementations.
 */
public class MobTypeBase implements MobType
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getName()
   */
  @Override
  public String getName()
  {
    return _name;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getDisplayName()
   */
  @Override
  public String getDisplayName()
  {
    return _displayName;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getMob()
   */
  @Override
  public String getMob()
  {
    return _mob;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getProbabilityWeight()
   */
  @Override
  public float getProbabilityWeight()
  {
    return _probabilityWeight;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canPlayerBecome(org.bukkit.entity.Player)
   * 
   *      Currently we're just checking the permission node, but in the future
   *      it we could check a set of known mob types and only allow a player to
   *      become a mob after he has "learned" it.
   */
  @Override
  public boolean canPlayerBecome(Player player)
  {
    return player.hasPermission("become.mob." + getName());
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getFullPower()
   */
  @Override
  public float getFullPower()
  {
    return _fullPower;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getPowerRegenRate()
   */
  @Override
  public float getPowerRegenRate()
  {
    return _powerRegenRate;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canPlace(org.bukkit.Material)
   */
  @Override
  public boolean canPlace(Material material)
  {
    return _placeableMaterials.contains(material);
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canBreak(org.bukkit.Material)
   */
  @Override
  public boolean canBreak(Material material)
  {
    return _breakableMaterials.contains(material);
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canInteract(org.bukkit.Material)
   */
  @Override
  public boolean canInteract(Material material)
  {
    return !_nonInteractableMaterials.contains(material);
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseItem(org.bukkit.Material)
   */
  @Override
  public boolean canUseItem(Material material)
  {
    return !_blockedItems.contains(material);
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseSword(org.bukkit.inventory.ItemStack)
   */
  @Override
  public boolean canUseSword(ItemStack itemInHand)
  {
    return !_blockedSwords.contains(itemInHand.getType());
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseBow()
   */
  @Override
  public boolean canUseBow()
  {
    return _canUseBow;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canUseTool(org.bukkit.inventory.ItemStack)
   */
  @Override
  public boolean canUseTool(ItemStack itemInHand)
  {
    return !_blockedTools.contains(itemInHand.getType());
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canWearArmour(org.bukkit.inventory.ItemStack)
   */
  @Override
  public boolean canWearArmour(ItemStack armour)
  {
    return !_blockedArmour.contains(armour.getType());
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#canFly()
   */
  @Override
  public boolean canFly()
  {
    return _canFly;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#isHurtByWater()
   */
  @Override
  public boolean isHurtByWater()
  {
    return _hurtByWater;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#isHurtBySun()
   */
  @Override
  public boolean isHurtBySun()
  {
    return _hurtBySun;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getWalkSpeed()
   */
  @Override
  public float getWalkSpeed()
  {
    return _walkSpeed;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getFlySpeed()
   */
  @Override
  public float getFlySpeed()
  {
    return _flySpeed;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getIdleSound()
   */
  @Override
  public Sound getIdleSound()
  {
    return _idleSound;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getHurtSound()
   */
  @Override
  public Sound getHurtSound()
  {
    return _hurtSound;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getChargeSound()
   */
  @Override
  public Sound getChargeSound()
  {
    return _chargeSound;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getAngrySound()
   */
  @Override
  public Sound getAngrySound()
  {
    return _angrySound;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialSound()
   */
  @Override
  public Sound getSpecialSound()
  {
    return _specialSound;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getDeathSound()
   */
  @Override
  public Sound getDeathSound()
  {
    return _deathSound;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSoundVolume()
   */
  @Override
  public float getSoundVolume()
  {
    return _soundVolume;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSoundCoolDown()
   */
  @Override
  public int getSoundCoolDown()
  {
    return _soundCoolDown;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getTalisman()
   */
  @Override
  public Material getTalisman()
  {
    return _talisman;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialPowerCost()
   */
  @Override
  public float getSpecialPowerCost()
  {
    return _specialPowerCost;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialRange()
   */
  @Override
  public float getSpecialRange()
  {
    return _specialRange;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialMultiplier()
   */
  @Override
  public float getSpecialMultiplier()
  {
    return _specialMultiplier;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialDamage()
   */
  @Override
  public double getSpecialDamage()
  {
    return _specialDamage;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialPotionEffects()
   */
  @Override
  public HashSet<PotionEffect> getSpecialPotionEffects()
  {
    return _specialPotionEffects;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getSpecialCoolDown()
   */
  @Override
  public int getSpecialCoolDown()
  {
    return _specialCoolDownTicks;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getPotionBuffs()
   */
  @Override
  public HashSet<PotionEffect> getPotionBuffs()
  {
    return _potions;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getWaterDamagePerSecond()
   */
  @Override
  public double getWaterDamagePerSecond()
  {
    return _waterDamagePerSecond;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#getAttackPotionEffects()
   */
  @Override
  public HashSet<PotionEffect> getAttackPotionEffects()
  {
    return _attackPotionEffects;
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onWaterDamage()
   */
  @Override
  public void onWaterDamage(MobPlayer mobPlayer)
  {
    Player player = mobPlayer.getPlayer();
    player.damage(_waterDamagePerSecond);
    if (player.getHealth() > 0)
    {
      for (PotionEffect effect : _waterDamagePotionEffects)
      {
        // Force is true to reset the clock to full duration.
        player.addPotionEffect(effect, true);
      }
    }
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity livingEntity)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onPlayerTransformTo(io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void onPlayerTransformTo(MobPlayer mobPlayer)
  {
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#showTransformToEffects(org.bukkit.plugin.Plugin,
   *      io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void showTransformToEffects(Plugin plugin, MobPlayer mobPlayer)
  {
    Location centre = mobPlayer.getPlayer().getLocation();
    if (_onBecomeLightning)
    {
      Effects.strikeLightningEffect(plugin, centre, 0);
    }
    if (_onBecomeParticles)
    {
      Effects.expandingFireRing(plugin, centre, 10, 30);
    }
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onPlayerTransformFrom(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.event.Event)
   */
  @Override
  public void onPlayerTransformFrom(MobPlayer player)
  {
    // Empty.
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#showTransformFromEffects(org.bukkit.plugin.Plugin,
   *      io.github.totemo.mobcombat.MobPlayer)
   */
  @Override
  public void showTransformFromEffects(Plugin plugin, MobPlayer mobPlayer)
  {
    Location centre = mobPlayer.getPlayer().getLocation();
    if (_onUnbecomeLightning)
    {
      Effects.strikeLightningEffect(plugin, centre, 0);
    }
    if (_onUnbecomeParticles)
    {
      Effects.expandingFireRing(plugin, centre, 10, 30);
    }
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onPlayerDeath(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.event.entity.PlayerDeathEvent)
   */
  @Override
  public void onPlayerDeath(MobPlayer player, PlayerDeathEvent event)
  {
    // TODO: fiddle with the drops to add mob-type specific items.
  }

  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#loadConfiguration(org.bukkit.configuration.ConfigurationSection,
   *      java.util.logging.Logger)
   */
  @Override
  public void loadConfiguration(ConfigurationSection section, Logger logger)
  {
    // Main attributes.
    ConfigHelper config = new ConfigHelper(logger);
    _name = section.getName();
    _displayName = section.getString("display_name", "a " + _name.toLowerCase());
    _mob = section.getString("mob", null);
    if (_mob == null)
    {
      logger.warning("for " + ConfigHelper.getFullPath(section, "mob") + ", defaulting the mob to " + getName());
    }
    _probabilityWeight = Math.max(0.0f, (float) section.getDouble("probability_weight", 1.0));

    // Statistics section.
    _fullPower = (float) section.getDouble("statistics.full_power", 1.0);
    _powerRegenRate = (float) section.getDouble("statistics.regen_rate", 0.1);
    _walkSpeed = (float) section.getDouble("statistics.walk_speed", 0.2);
    _flySpeed = (float) section.getDouble("statistics.fly_speed", 0.2);
    _idleSound = config.loadSound(section, "sound.idle_sound", null, true);
    _hurtSound = config.loadSound(section, "sound.hurt_sound", Sound.HURT_FLESH, true);
    _chargeSound = config.loadSound(section, "sound.charge_sound", null, true);
    _angrySound = config.loadSound(section, "sound.angry_sound", null, true);
    _specialSound = config.loadSound(section, "sound.special_sound", null, true);
    _deathSound = config.loadSound(section, "sound.death_sound", null, true);
    _soundVolume = Math.max(1.0f, (float) section.getDouble("sound.volume", 50.0));
    _soundCoolDown = Math.max(10, section.getInt("sound.cool_down"));

    // Capabilities section.
    _placeableMaterials = config.loadMaterials(section, "capabilities.can_place", false);
    _breakableMaterials = config.loadMaterials(section, "capabilities.can_break", false);
    _nonInteractableMaterials = config.loadMaterials(section, "capabilities.can_not_interact", false);
    _blockedItems = config.loadMaterials(section, "capabilities.can_not_use", false);

    _blockedSwords = new HashSet<Material>(InventoryHelper.SWORD);
    _blockedSwords.removeAll(config.loadMaterials(section, "capabilities.allowed_swords", false));;
    _blockedTools = new HashSet<Material>(InventoryHelper.TOOL);
    _blockedTools.removeAll(config.loadMaterials(section, "capabilities.allowed_tools", false));
    _blockedArmour = new HashSet<Material>(InventoryHelper.ARMOUR);
    _blockedArmour.removeAll(config.loadMaterials(section, "capabilities.allowed_armour", false));
    _canUseBow = section.getBoolean("capabilities.can_use_bow", false);
    _canFly = section.getBoolean("capabilities.can_fly", false);
    _hurtByWater = section.getBoolean("capabilities.hurt_by_water", false);
    _hurtBySun = section.getBoolean("capabilities.hurt_by_sun", false);

    // Special section.
    _talisman = config.loadMaterial(section, "special.talisman", Material.DIAMOND_BLOCK, true);
    _specialPowerCost = (float) section.getDouble("special.power_cost", 1.0);
    _specialRange = (float) section.getDouble("special.range", 50);
    _specialMultiplier = (float) section.getDouble("special.multiplier", 1.0);
    _specialDamage = Math.max(0, section.getDouble("special.damage", 0.0));
    _specialPotionEffects = config.loadPotions(section, "special.potions", false);
    _specialWarmUpTicks = Math.max(0, section.getInt("special.warm_up", 0));
    _specialCoolDownTicks = Math.max(0, section.getInt("special.cool_down", 40));

    // Potions section.
    _potions = config.loadPotions(section, "potions", true);

    // Water damage effects.
    _waterDamagePerSecond = Math.max(0, section.getDouble("hurt_by_water.damage", 1.0));
    _waterDamagePotionEffects = config.loadPotions(section, "hurt_by_water.potions", true);

    // Become section.
    _onBecomeLightning = section.getBoolean("become.lightning", true);
    _onBecomeParticles = section.getBoolean("become.particles", true);
    _onUnbecomeLightning = section.getBoolean("unbecome.lightning", true);
    _onUnbecomeParticles = section.getBoolean("unbecome.particles", true);

    // Attack section.
    _attackPotionEffects = config.loadPotions(section, "attack.potions", false);

  } // loadConfiguration

  // --------------------------------------------------------------------------
  /**
   * The programmatic name.
   */
  protected String                _name;

  /**
   * The display name.
   */
  protected String                _displayName;

  /**
   * The mob to use for the disguise.
   */
  protected String                _mob;

  /**
   * The relative weighting of this mob in random selections.
   */
  protected float                 _probabilityWeight;

  /**
   * Full power level, typically 1.0 (1 XP level).
   */
  protected float                 _fullPower;

  /**
   * Power regeneration rate (XP levels per second).
   */
  protected float                 _powerRegenRate;

  /**
   * Walking speed, [-1.0, 1.0].
   */
  protected float                 _walkSpeed;

  /**
   * Flying speed, [-1.0, 1.0].
   */
  protected float                 _flySpeed;

  /**
   * Regularly played idle sound.
   */
  protected Sound                 _idleSound;

  /**
   * Sound played when hurt.
   */
  protected Sound                 _hurtSound;

  /**
   * Sound played when angry. Activated by right click talisman while sprinting.
   */
  protected Sound                 _chargeSound;

  /**
   * Sound played when angry. Activated by right click talisman while sneaking.
   */
  protected Sound                 _angrySound;

  /**
   * Sound played when special ability is used.
   */
  protected Sound                 _specialSound;

  /**
   * Sound played when the mob dies.
   */
  protected Sound                 _deathSound;

  /**
   * The volume of the mob's sound effects.
   */
  protected float                 _soundVolume;

  /**
   * Minimum number of ticks that must elapse before sounds can be voluntarily
   * played (charge and angry sounds). Just to prevent spam.
   */
  protected int                   _soundCoolDown;

  /**
   * Set of block materials that the mob can place.
   */
  protected HashSet<Material>     _placeableMaterials       = new HashSet<Material>();

  /**
   * Set of block materials that the mob can break.
   */
  protected HashSet<Material>     _breakableMaterials       = new HashSet<Material>();

  /**
   * Set of block materials that the mob can interact with.
   */
  protected HashSet<Material>     _nonInteractableMaterials = new HashSet<Material>();

  /**
   * Set of items that the mob can not use.
   */
  protected HashSet<Material>     _blockedItems             = new HashSet<Material>();

  /**
   * Materials signifying blocked sword types.
   */
  protected HashSet<Material>     _blockedSwords            = new HashSet<Material>();

  /**
   * Materials signifying blocked tool types.
   */
  protected HashSet<Material>     _blockedTools             = new HashSet<Material>();

  /**
   * Materials signifying blocked armour types.
   */
  protected HashSet<Material>     _blockedArmour            = new HashSet<Material>();

  /**
   * True if the mob can use a bow.
   */
  protected boolean               _canUseBow;

  /**
   * True if the mob can fly.
   */
  protected boolean               _canFly;

  /**
   * True if the mob is hurt by water.
   */
  protected boolean               _hurtByWater;

  /**
   * True if the mob is hurt by direct sunlight.
   */
  protected boolean               _hurtBySun;

  /**
   * The magic item that unlocks this mob's special ability.
   */
  protected Material              _talisman;

  /**
   * Power cost of using the special attack.
   */
  protected float                 _specialPowerCost;

  /**
   * Range value for the special power, if relevant.
   */
  protected float                 _specialRange;

  /**
   * Effect multiplier for the special power, e.g explosive force, if relevant.
   */
  protected float                 _specialMultiplier;

  /**
   * Special attack damage when right clicking on a LivingEntity.
   */
  protected double                _specialDamage;

  /**
   * Special attack potion effects when right clicking on a LivingEntity.
   */
  protected HashSet<PotionEffect> _specialPotionEffects;

  /**
   * Number of ticks before special attack takes effect.
   */
  protected float                 _specialWarmUpTicks;

  /**
   * Number of ticks after special attack before it can be used again.
   */
  protected int                   _specialCoolDownTicks;

  /**
   * The set of potion effects to be applied to the mob. Non-null reference.
   */
  protected HashSet<PotionEffect> _potions;

  /**
   * Damage in hearts per second of water contact.
   */
  protected double                _waterDamagePerSecond;

  /**
   * The set of damaging potion effects applied to mobs attacked by this type.
   */
  protected HashSet<PotionEffect> _attackPotionEffects;

  /**
   * Potion effects renewed by water contact.
   */
  protected HashSet<PotionEffect> _waterDamagePotionEffects = new HashSet<PotionEffect>();

  /**
   * Health cost of becoming this mob in half-hearts.
   */
  protected int                   _becomeHealthCost;

  /**
   * Power cost of becoming this mob.
   */
  protected float                 _becomePowerCost;

  /**
   * If true, do a damageless lightning strike on becoming the mob.
   */
  protected boolean               _onBecomeLightning;

  /**
   * If true, do a snazzy particle effect on becoming the mob.
   */
  protected boolean               _onBecomeParticles;
  /**
   * Health cost of un-becoming this mob in half-hearts.
   */
  protected int                   _unbecomeHealthCost;

  /**
   * If true, do a damageless lightning strike on un-becoming the mob.
   */
  protected boolean               _onUnbecomeLightning;

  /**
   * If true, do a snazzy particle effect on un-becoming the mob.
   */
  protected boolean               _onUnbecomeParticles;

} // class MobTypeBase
