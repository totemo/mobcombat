package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

// --------------------------------------------------------------------------
/**
 * Custom implementation details for the Spider mob type.
 */
public class MobTypeSpider extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();

    // If the spider can see it, it can jump towards it.
    List<Block> blocks = player.getLastTwoTargetBlocks(null, (int) mobType.getSpecialRange());
    if (blocks.size() == 2)
    {
      Block last = blocks.get(1);
      Location dest = last.getLocation();
      Location loc = player.getLocation();
      Vector direction = new Vector(dest.getX() - loc.getX(),
                                    dest.getY() - loc.getY(),
                                    dest.getZ() - loc.getZ());
      direction.multiply(mobType.getSpecialMultiplier() / direction.length());
      direction.add(player.getVelocity());
      player.setVelocity(direction);
      mobPlayer.addPower(-mobType.getSpecialPowerCost());
      mobPlayer.playSoundInWorld(mobType.getSpecialSound());
    }

    // In addition to jumping at them, apply attack potion effects. Plus damage.
    if (living != null)
    {
      living.damage(mobType.getSpecialDamage(), player);
      for (PotionEffect potion : mobType.getSpecialPotionEffects())
      {
        living.addPotionEffect(potion, true);
      }
    }
  } // onSpecialAbility
} // class MobTypeSpider
