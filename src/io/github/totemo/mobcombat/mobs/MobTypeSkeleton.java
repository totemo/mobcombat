package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

// ----------------------------------------------------------------------------
/**
 * Custom implementation details for the Skeleton mob type.
 */
public class MobTypeSkeleton extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();
    mobPlayer.addPower(-mobType.getSpecialPowerCost());
    mobPlayer.playSoundInWorld(mobType.getSpecialSound());
    Arrow projectile = player.launchProjectile(Arrow.class);
    projectile.setVelocity(projectile.getVelocity().multiply(mobType.getSpecialMultiplier()));
    projectile.setFireTicks(200);
    projectile.setShooter(player);
  }
}
// class MobTypeSkeleton