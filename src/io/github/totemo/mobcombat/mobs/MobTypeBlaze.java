package io.github.totemo.mobcombat.mobs;

import io.github.totemo.mobcombat.MobPlayer;

import org.bukkit.Effect;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;

// --------------------------------------------------------------------------
/**
 * Custom implementation details for the Blaze mob type.
 */
public class MobTypeBlaze extends MobTypeBase
{
  // --------------------------------------------------------------------------
  /**
   * @see io.github.totemo.mobcombat.mobs.MobType#onSpecialAbility(io.github.totemo.mobcombat.MobPlayer,
   *      org.bukkit.entity.LivingEntity)
   */
  @Override
  public void onSpecialAbility(MobPlayer mobPlayer, LivingEntity living)
  {
    Player player = mobPlayer.getPlayer();
    MobType mobType = mobPlayer.getMobType();
    SmallFireball fireball = player.launchProjectile(SmallFireball.class);
    fireball.setYield(fireball.getYield() * mobType.getSpecialMultiplier());
    fireball.setShooter(player);
    mobPlayer.addPower(-mobType.getSpecialPowerCost());
    player.playEffect(player.getLocation(), Effect.BLAZE_SHOOT, 0);
  }
} // class MobTypeBlaze