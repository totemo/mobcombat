package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.mobs.MobType;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import pgDev.bukkit.DisguiseCraft.DisguiseCraft;
import pgDev.bukkit.DisguiseCraft.api.DisguiseCraftAPI;
import pgDev.bukkit.DisguiseCraft.disguise.Disguise;
import pgDev.bukkit.DisguiseCraft.disguise.DisguiseType;

// ----------------------------------------------------------------------------
/**
 * A wrapper around the plugin used to disguise players as a mob.
 * 
 * Currently written to use DisguiseCraft (despite the name).
 */
public class MobDisguiseAPI
{
  // ---------------------------------------------------------------------------
  /**
   * This should be called in the Plugin's onEnable() method to check for the
   * availability of the mob disguise API.
   * 
   * @param plugin the calling plugin.
   */
  public void onEnable(Plugin plugin)
  {
    _plugin = plugin;

    // Is the mob disguise plugin available for use?
    _available = false;
    try
    {
      @SuppressWarnings("unused")
      DisguiseCraftAPI api = DisguiseCraft.getAPI();
      _available = true;
      _plugin.getLogger().info("The DisguiseCraft plugin is available.");
    }
    catch (Exception ex)
    {
      _plugin.getLogger().severe("The DisguiseCraft plugin is not enabled. Players won't be able to turn into mobs.");
    }
  }

  // ---------------------------------------------------------------------------
  /**
   * Return true if the mob disguise plugin is available for use (basically, is
   * it installed?).
   * 
   * @return true if the mob disguise plugin is available for use (basically, is
   *         it installed?).
   */
  public boolean isAvailable()
  {
    return _available;
  }

  // ---------------------------------------------------------------------------
  /**
   * Return the canonical name of the mob type of the player's current disguise,
   * or MobType.HUMAN if not disguised.
   * 
   * @return the canonical name of the mob type of the player's current
   *         disguise, or MobType.HUMAN if not disguised.
   */
  public String getDisguise(Player player)
  {
    if (isAvailable())
    {
      try
      {
        DisguiseCraftAPI api = DisguiseCraft.getAPI();
        Disguise disguise = api.getDisguise(player);
        return (disguise != null) ? disguise.type.name() : MobType.HUMAN.getName();
      }
      catch (Exception ex)
      {
        // Silent.
      }
    }
    return MobType.HUMAN.getName();
  }

  // ---------------------------------------------------------------------------
  /**
   * Set the current disguise of the player.
   * 
   * @param player the player.
   * @param mob the mob type, case insensitive. Null reference, Human and Player
   *          (case insensitive) all signify an undisguised player.
   */
  public void setDisguise(Player player, String mob)
  {
    if (isAvailable())
    {
      try
      {
        DisguiseCraftAPI api = DisguiseCraft.getAPI();
        Disguise disguise = api.getDisguise(player);

        if (mob == null || mob.equalsIgnoreCase(MobType.HUMAN.getName()) || mob.equalsIgnoreCase("Player"))
        {
          api.undisguisePlayer(player);
        }
        else
        {
          Disguise newDisguise;
          if (mob.equalsIgnoreCase("WitherSkeleton"))
          {
            newDisguise = new Disguise(api.newEntityID(), "wither", DisguiseType.Skeleton);
          }
          else
          {
            // DisguiseCraft docs: "It is better to clone and edit an existing
            // disguise than to create a new one."
            DisguiseType newDisguiseType = DisguiseType.fromString(mob);
            if (disguise != null)
            {
              newDisguise = disguise.clone();
              newDisguise.setType(newDisguiseType);
            }
            else
            {
              newDisguise = new Disguise(api.newEntityID(), newDisguiseType);
            }
          }
          api.changePlayerDisguise(player, newDisguise);
        }
      }
      catch (Exception ex)
      {
        // Silent.
      }
    }
  } // setCurrentDisguise

  // --------------------------------------------------------------------------
  /**
   * The owning plugin.
   */
  protected Plugin  _plugin;

  /**
   * True if the mob disguise plugin is available for use.
   */
  protected boolean _available;
} // class MobDisguiseAPI