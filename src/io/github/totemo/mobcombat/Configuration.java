package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.util.ConfigHelper;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

// ----------------------------------------------------------------------------
/**
 * Holds configuration settings.
 */
public class Configuration
{
  // --------------------------------------------------------------------------
  /**
   * Constructor.
   * 
   * @param plugin the associated plugin instance.
   */
  public Configuration(MobCombat plugin)
  {
    _plugin = plugin;
  }

  // --------------------------------------------------------------------------
  /**
   * Save the configuration file.
   */
  public void save()
  {
    _plugin.saveConfig();
  }

  // --------------------------------------------------------------------------
  /**
   * Load the configuration file.
   */
  public void load()
  {
    _plugin.reloadConfig();
    _plugin.getMobTypeRegistry().loadMobTypes(_plugin.getConfig(), _plugin.getLogger());

    ConfigurationSection general = _plugin.getConfig().getConfigurationSection("general");
    _mobTransformationMessage = general.getStringList("mob_transformation_message");

    ConfigHelper config = new ConfigHelper(_plugin.getLogger());
    _healingItem = config.loadMaterial(general, "healing_item", Material.GOLDEN_APPLE, false);
    _cheapMaterials = config.loadMaterials(general, "cheap_materials", true);

    _materialValues.clear();
    int materialValue = 1;
    for (Material material : _cheapMaterials)
    {
      _materialValues.put(material, materialValue++);
    }

    _sunburnTicks = Math.max(0, general.getInt("sunburn_ticks"));
    _sunRise = Math.min(24000, Math.max(0, general.getInt("sunrise", 6000)));
    _sunSet = Math.min(24000, Math.max(0, general.getInt("sunset", 18000)));
    _pearlDamage = Math.max(0, general.getDouble("pearl_damage"));
    _plugin.getRegionTracker().load(_plugin.getConfig().getConfigurationSection("regions"));
  } // load

  // --------------------------------------------------------------------------
  /**
   * Return the generic message sent to the player after being transformed into
   * a mob.
   * 
   * @return the generic message sent to the player after being transformed into
   *         a mob.
   */
  public List<String> getMobTransformationMessage()
  {
    return _mobTransformationMessage;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the item type that when eaten transforms a mob back into a human.
   * 
   * @return the item type that when eaten transforms a mob back into a human.
   */
  public Material getHealingItem()
  {
    return _healingItem;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the set of Materials considered cheap for the purposes of dropping
   * items from a player's inventory in order to make room.
   * 
   * Materials are listed in ascending order of value.
   * 
   * @return the set of Materials considered cheap for the purposes of dropping
   *         items from a player's inventory in order to make room.
   */
  public LinkedHashSet<Material> getCheapMaterials()
  {
    return _cheapMaterials;
  }

  // --------------------------------------------------------------------------
  /**
   * Return a map from Material to the corresponding relative value ordinal,
   * starting at 1 for the cheapest material and increasing by one for each
   * subsequent material of higher value.
   * 
   * The value 0 is reserved for use with empty slots.
   * 
   * @return a map from Material to the corresponding relative value ordinal,
   *         starting at 1 for the cheapest material and increasing by one for
   *         each subsequent material of higher value.
   */
  protected HashMap<Material, Integer> getMaterialValues()
  {
    return _materialValues;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the number of fire ticks to apply to mobs that are hurt by direct
   * sunlight.
   * 
   * @return the number of fire ticks to apply to mobs that are hurt by direct
   *         sunlight.
   */
  public int getSunburnTicks()
  {
    return _sunburnTicks;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the World.getTime() value of sunrise.
   * 
   * @return the World.getTime() value of sunrise.
   */
  public int getSunrise()
  {
    return _sunRise;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the World.getTime() value of sunset.
   * 
   * @return the World.getTime() value of sunset.
   */
  public int getSunset()
  {
    return _sunSet;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the additional ender pearl damage applied to non-endermen to prevent
   * people from pearling out of minigame arenas.
   * 
   * @return the additional ender pearl damage applied to non-endermen.
   */
  public double getPearlDamage()
  {
    return _pearlDamage;
  }

  // --------------------------------------------------------------------------
  /**
   * Reference to the plugin instance.
   */
  protected MobCombat                  _plugin;

  /**
   * The generic message sent to the player after being transformed into a mob.
   */
  protected List<String>               _mobTransformationMessage;

  /**
   * The special food that transforms a mob back into a human.
   */
  protected Material                   _healingItem;

  /**
   * The set of Materials considered cheap for the purposes of dropping items
   * from a player's inventory to make room, listed in ascending order by value.
   */
  protected LinkedHashSet<Material>    _cheapMaterials;

  /**
   * Map from Material to the corresponding relative value ordinal, starting at
   * 0 for the cheapest material and increasing by one for each subsequent
   * material of higher value.
   */
  protected HashMap<Material, Integer> _materialValues = new HashMap<Material, Integer>();

  /**
   * The number of fire ticks to apply to mobs that are hurt by direct sunlight.
   */
  protected int                        _sunburnTicks;

  /**
   * The World.getTime() value of sunrise.
   */
  protected int                        _sunRise;

  /**
   * The World.getTime() value of sunset.
   */
  protected int                        _sunSet;

  /**
   * Additional ender pearl damage applied to non-endermen to prevent people
   * from pearling out of minigame arenas.
   */
  protected double                     _pearlDamage;
} // class Configuration