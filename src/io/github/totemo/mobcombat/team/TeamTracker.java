package io.github.totemo.mobcombat.team;

import io.github.totemo.mobcombat.MobPlayer;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

// ----------------------------------------------------------------------------
/**
 * Tracks membership of teams and ensures that team membership follows that of
 * the the main Scoreboard.
 * 
 * Since the Scoreboard API doesn't raise events relating to team membership, it
 * may be necessary to kick a player in order to update their team. Team
 * membership is synchronised to the main Scoreboard on the following events:
 * <ul>
 * <li>Player join.</li>
 * <li>Player leave.</li>
 * <li>Player kick.</li>
 * <li>Player respawn.</li>
 * <li>Player teleport, except when an ender pearl is used.</li>
 * </ul>
 */
public class TeamTracker implements Listener
{
  /**
   * Constructor.
   * 
   * @param plugin the owning plugin, for use of the logger, etc.
   */
  public TeamTracker(JavaPlugin plugin)
  {
    _plugin = plugin;
  }

  // --------------------------------------------------------------------------
  /**
   * Initialise the set of teams from the scoreboard API.
   */
  public void initTeams()
  {
    ScoreboardManager manager = Bukkit.getServer().getScoreboardManager();
    Scoreboard scoreboard = manager.getMainScoreboard();
    for (Team team : scoreboard.getTeams())
    {
      for (OfflinePlayer player : team.getPlayers())
      {
        // Update the mapping for online players only; update the rest at login.
        if (player.isOnline())
        {
          updateMapping(player);
        }
      }
    }
  } // initTeams

  // --------------------------------------------------------------------------
  /**
   * Return the MobPlayer corresponding to the specified player.
   * 
   * @return the MobPlayer corresponding to the specified player.
   */
  public MobPlayer getMobPlayer(OfflinePlayer player)
  {
    MobPlayer mobPlayer = _players.get(player.getName());
    if (mobPlayer == null)
    {
      mobPlayer = new MobPlayer(player);
      mobPlayer.setMobTeam(getMobTeam(getTeamOfPlayer(player)));
      _players.put(player.getName(), mobPlayer);
    }
    return mobPlayer;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the MobTeam corresponding to the specified Bukkit Team.
   * 
   * The MobTeam will be created on demand.
   * 
   * @return the MobTeam corresponding to the specified Bukkit Team.
   */
  public MobTeam getMobTeam(Team team)
  {
    if (team == null)
    {
      return null;
    }

    MobTeam mobTeam = _teams.get(team.getName());
    if (mobTeam == null)
    {
      mobTeam = new MobTeam(team.getName(), team.getDisplayName());
      _teams.put(mobTeam.getName(), mobTeam);
    }
    return mobTeam;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the MobTeam with the specified name.
   * 
   * @return the MobTeam with the specified name.
   */
  public MobTeam getMobTeam(String name)
  {
    return _teams.get(name);
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerJoinEvent(PlayerJoinEvent event)
  {
    Player player = event.getPlayer();
    updateMapping(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerKickEvent(PlayerKickEvent event)
  {
    Player player = event.getPlayer();
    updateMapping(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerQuitEvent(PlayerQuitEvent event)
  {
    Player player = event.getPlayer();
    updateMapping(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerRespawnEvent(PlayerRespawnEvent event)
  {
    Player player = event.getPlayer();
    updateMapping(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Update the cached mapping from player name to corresponding TeamMemberInfo.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerTeleportEvent(PlayerTeleportEvent event)
  {
    Player player = event.getPlayer();
    if (event.getCause() != TeleportCause.ENDER_PEARL)
    {
      updateMapping(player);
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Convenience method to return the Scoreboard API Team of a player.
   * 
   * @param player the player.
   * @retuen the corresponding team or null if not set.
   */
  protected Team getTeamOfPlayer(OfflinePlayer player)
  {
    ScoreboardManager manager = Bukkit.getServer().getScoreboardManager();
    Scoreboard scoreboard = manager.getMainScoreboard();
    return scoreboard.getPlayerTeam(player);
  }

  // --------------------------------------------------------------------------
  /**
   * Associate team and mob related state information with the player.
   * 
   * @param player the player.
   */
  protected void updateMapping(OfflinePlayer player)
  {
    MobPlayer mobPlayer = getMobPlayer(player);
    Team team = getTeamOfPlayer(player);
    MobTeam mobTeam = getMobTeam(team);
    _plugin.getLogger().info("Assigning " + player.getName() + " to " + mobTeam.getDisplayName());
    mobPlayer.setMobTeam(mobTeam);
  }

  // --------------------------------------------------------------------------
  /**
   * The owning plugin.
   */
  protected JavaPlugin                 _plugin;

  /**
   * Map from team name to MobTeam instance.
   */
  protected HashMap<String, MobTeam>   _teams   = new HashMap<String, MobTeam>();

  /**
   * Map from player name to MobPlayer instance.
   */
  protected HashMap<String, MobPlayer> _players = new HashMap<String, MobPlayer>();
} // class TeamTracker