package io.github.totemo.mobcombat.team;

import io.github.totemo.mobcombat.MobPlayer;

import java.util.HashMap;

// ----------------------------------------------------------------------------
/**
 * Represents one team of players.
 */
public class MobTeam
{
  /**
   * Constructor.
   * 
   * @param name the internal (not displayed) team name.
   * @param displayName the display name of this team.
   */
  public MobTeam(String name, String displayName)
  {
    _name = name;
    _displayName = displayName;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the internal name of this team.
   * 
   * @return the internal name of this team.
   */
  public String getName()
  {
    return _name;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the display name of this team.
   * 
   * @return the display name of this team.
   */
  public String getDisplayName()
  {
    return _displayName;
  }

  // --------------------------------------------------------------------------
  /**
   * Add the player to this team.
   * 
   * Do not call directly. Use {@link MobPlayer#setMobTeam(MobTeam)} instead.
   * 
   * @param player the player.
   */
  public void addPlayer(MobPlayer player)
  {
    _players.put(player.getOfflinePlayer().getName(), player);
  }

  // --------------------------------------------------------------------------
  /**
   * Remove the player from this team.
   * 
   * Do not call directly. Use {@link MobPlayer#setMobTeam(MobTeam)} instead.
   * 
   * @param player the player.
   */
  public void removePlayer(MobPlayer player)
  {
    _players.remove(player.getOfflinePlayer().getName());
  }

  // --------------------------------------------------------------------------
  /**
   * The internal team name; not displayed.
   */
  protected String                     _name;

  /**
   * The displayed name.
   */
  protected String                     _displayName;

  /**
   * The players in this team.
   */
  protected HashMap<String, MobPlayer> _players = new HashMap<String, MobPlayer>();
} // class MobTeam