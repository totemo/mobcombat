package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.mobs.MobType;
import io.github.totemo.mobcombat.mobs.MobTypeRegistry;
import io.github.totemo.mobcombat.region.RegionTracker;
import io.github.totemo.mobcombat.region.TransformationRegion;
import io.github.totemo.mobcombat.util.EnvironmentHelper;
import io.github.totemo.mobcombat.util.InventoryHelper;
import io.github.totemo.mobcombat.util.Messages;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import com.amoebanman.kitmaster.utilities.CommandController;

// ----------------------------------------------------------------------------
/**
 * Plugin class.
 */
public class MobCombat extends JavaPlugin implements Listener, Runnable
{
  // --------------------------------------------------------------------------
  /**
   * Constructor.
   * 
   * Enable nagging about screwups.
   */
  public MobCombat()
  {
    setNaggable(true);
  }

  // --------------------------------------------------------------------------
  /**
   * Return the externally accessible MobCombat API.
   * 
   * @return the externally accessible MobCombat API.
   */
  public API getAPI()
  {
    return _api;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the mob disguise API used to change the appearance of players.
   * 
   * @return the mob disguise API used to change the appearance of players.
   */
  public MobDisguiseAPI getMobDisguiseAPI()
  {
    return _mobDisguiseAPI;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the {@link MobTypeRegistry}.
   * 
   * @return the {@link MobTypeRegistry}.
   */
  public MobTypeRegistry getMobTypeRegistry()
  {
    return _mobTypeRegistry;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the {@link MobPlayerTracker}.
   * 
   * @return the {@link MobPlayerTracker}.
   */
  public MobPlayerTracker getMobPlayerTracker()
  {
    return _mobPlayerTracker;
  }

  // --------------------------------------------------------------------------
/**
   * Return the {@link RegionTracker).
   * 
   * @return the {@link RegionTracker).
   */
  public RegionTracker getRegionTracker()
  {
    return _regionTracker;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the configuration.
   * 
   * @return the configuration.
   */
  public Configuration getConfiguration()
  {
    return _configuration;
  }

  // --------------------------------------------------------------------------
  /**
   * On enabling, load the configuration, set up the teams, set up the command
   * handler and timer.
   * 
   * Renew player mob disguises on a reload.
   */
  @Override
  public void onEnable()
  {
    // Saves only if config.yml doesn't exist.
    saveDefaultConfig();
    _configuration.load();

    getServer().getPluginManager().registerEvents(this, this);
    getServer().getPluginManager().registerEvents(_mobPlayerTracker, this);

    // The Plugin.getLogger() (used by help) is null at plugin construction.
    if (_commands == null)
    {
      _commands = new Commands(this);
    }
    CommandController.registerCommands(this, _commands);

    _mobDisguiseAPI.onEnable(this);
    _mobPlayerTracker.onEnable();
    if (_mobDisguiseAPI.isAvailable())
    {
      getServer().getScheduler().scheduleSyncRepeatingTask(this, this, 0, ONE_SECOND_TICKS);
    }
  } // onEnable

  // --------------------------------------------------------------------------
  /**
   * On disabling, cancel all scheduler tasks, clear out the player tracker.
   */
  @Override
  public void onDisable()
  {
    getServer().getScheduler().cancelTasks(this);
    _mobPlayerTracker.onDisable();
  }

  // --------------------------------------------------------------------------
  /**
   * Inflict extra damage when non-endermen teleport with ender pearls.
   * 
   * This is necessary to prevent players from pearling out of minigame arenas
   * by spamming pearls. It's not necessary if you can exactly protect the arena
   * boundary with WorldGuard entry: deny and enderpearl: deny regions.
   */
  @EventHandler(ignoreCancelled = true)
  public void onPlayerTeleport(PlayerTeleportEvent event)
  {
    if (event.getCause() == TeleportCause.ENDER_PEARL)
    {
      TransformationRegion region = getRegionTracker().getTransformationRegion(event.getFrom());
      if (region != null)
      {
        Player player = event.getPlayer();
        MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
        if (!mobPlayer.getMobType().getName().equals("Enderman"))
        {
          player.damage(getConfiguration().getPearlDamage());
        }
      }
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to build according to MobType capabilities.
   */
  @SuppressWarnings("deprecation")
  @EventHandler(ignoreCancelled = true)
  public void onBlockPlace(BlockPlaceEvent event)
  {
    Player player = event.getPlayer();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    MobType mobType = mobPlayer.getMobType();
    if (mobType == MobType.HUMAN)
    {
      return;
    }

    Material placedMaterial = (player.getItemInHand() != null) ? player.getItemInHand().getType() : null;
    if (!mobType.canPlace(placedMaterial))
    {
      Messages.success(player, "As " + mobType.getDisplayName() + " you can't place that.");
      event.setCancelled(true);
      player.updateInventory();
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to mine according to MobType capabilities.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onBlockBreak(BlockBreakEvent event)
  {
    Player player = event.getPlayer();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    MobType mobType = mobPlayer.getMobType();
    if (mobType == MobType.HUMAN)
    {
      return;
    }

    if (!mobType.canBreak(event.getBlock().getType()))
    {
      Messages.success(player, "As " + mobType.getDisplayName() + " you can't break that.");
      event.setCancelled(true);
    }
    else if (player.getItemInHand() != null && !mobType.canUseTool(player.getItemInHand()))
    {
      Messages.success(player, "As " + mobType.getDisplayName() + " you can't use that tool.");
      event.setCancelled(true);
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to interact with an entity.
   */
  @SuppressWarnings("deprecation")
  @EventHandler(ignoreCancelled = true)
  protected void onPlayerInteractEntity(PlayerInteractEntityEvent event)
  {
    Player player = event.getPlayer();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    MobType mobType = mobPlayer.getMobType();
    if (mobType == MobType.HUMAN)
    {
      return;
    }

    Material usedMaterial = (player.getItemInHand() != null) ? player.getItemInHand().getType() : null;
    if (usedMaterial == mobType.getTalisman() && event.getRightClicked() instanceof LivingEntity)
    {
      if (mobPlayer.canUseSpecialAbility())
      {
        mobType.onSpecialAbility(mobPlayer, (LivingEntity) event.getRightClicked());
      }
      else
      {
        // Frustrated at inability to use special.
        mobPlayer.playSoundInWorld(mobType.getAngrySound());
      }
      return;
    }

    if (usedMaterial != null && !mobType.canUseItem(usedMaterial))
    {
      mobPlayer.playSoundInWorld(mobType.getAngrySound());
      Messages.success(player, "As " + mobType.getDisplayName() + " you don't know how to use that.");
      event.setCancelled(true);
      player.updateInventory();
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to empty a bucket.
   */
  @SuppressWarnings("deprecation")
  @EventHandler(ignoreCancelled = true)
  public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event)
  {
    Player player = event.getPlayer();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    MobType mobType = mobPlayer.getMobType();
    if (mobType == MobType.HUMAN)
    {
      return;
    }

    Material bucket = event.getBucket();
    if (!mobType.canUseItem(bucket))
    {
      mobPlayer.playSoundInWorld(mobType.getAngrySound());
      Messages.success(player, "As " + mobType.getDisplayName() + " you don't know how to use a bucket.");
      event.setCancelled(true);
      player.updateInventory();
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to fill a bucket.
   */
  @EventHandler(ignoreCancelled = true)
  @SuppressWarnings("deprecation")
  public void onPlayerBucketFill(PlayerBucketFillEvent event)
  {
    Player player = event.getPlayer();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    MobType mobType = mobPlayer.getMobType();
    if (mobType == MobType.HUMAN)
    {
      return;
    }

    Material bucket = event.getBucket();
    if (!mobType.canUseItem(bucket))
    {
      mobPlayer.playSoundInWorld(mobType.getAngrySound());
      Messages.success(player, "As " + mobType.getDisplayName() + ", you don't know how to use a bucket.");
      event.setCancelled(true);
      player.updateInventory();
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to interact with an entity.
   * 
   * Notes:
   * <ol>
   * <li>When right clicking air, the event is delivered pre-cancelled (so don't
   * ignore cancelled events here), with the action set to RIGHT_CLICK_AIR.</li>
   * <li>When the player's hand is empty, the client doesn't send a packet to
   * the server (according to https://bukkit.atlassian.net/browse/BUKKIT-3677).</li>
   * <li>Spawn protection interacts with this event handler in ways that I don't
   * fully comprehend. I suspect that the events are not being fired. My
   * solution was to minimise the protection.</li>
   * </ol>
   */
  @SuppressWarnings("deprecation")
  @EventHandler(ignoreCancelled = false)
  protected void onPlayerInteract(PlayerInteractEvent event)
  {
    Player player = event.getPlayer();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    MobType mobType = mobPlayer.getMobType();
    if (mobType == MobType.HUMAN)
    {
      // No change to human capabilities.
      return;
    }

    if (event.getAction() == Action.LEFT_CLICK_AIR && (player.isSneaking() || player.isSprinting()))
    {
      mobPlayer.playSoundInWorld(mobType.getChargeSound());
    }
    else if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
    {
      ItemStack item = event.getItem();
      if (item != null && item.getType() == mobType.getTalisman())
      {
        event.setCancelled(true);
        player.updateInventory();

        // Make the angry sound if crouched while right-clicking with the
        // talisman, or if there is insufficient power to attack, or if the
        // special ability cool-down is still in effect.
        if (player.isSneaking() || !mobPlayer.canUseSpecialAbility())
        {
          mobPlayer.playSoundInWorld(mobType.getAngrySound());
        }
        else
        {
          mobType.onSpecialAbility(mobPlayer, null);
        }
        return;
      }
      else if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
      {
        if (!mobType.canInteract(event.getClickedBlock().getType()) ||
            item != null && !mobType.canUseItem(event.getItem().getType()))
        {
          mobPlayer.playSoundInWorld(mobType.getAngrySound());
          Messages.success(player, "As " + mobType.getDisplayName() + " you don't know how to use that.");
          event.setCancelled(true);
          player.updateInventory();
          // Redundant:
          // event.setUseInteractedBlock(Result.DENY);
          // event.setUseItemInHand(Result.DENY);
        }
      }
    }
  } // onPlayerInteract

  // --------------------------------------------------------------------------
  /**
   * Handle damage to a player when disguised as a mob.
   * 
   * Note: setting the last damage cause for a player does NOT work to associate
   * a player-launched fireball with a player's death. Code like the following
   * won't work:
   * 
   * <pre>
   * EntityDamageByEntityEvent modifiedDamageEvent = new EntityDamageByEntityEvent(shootingPlayer, player, DamageCause.PROJECTILE,
   *                                                                               event.getDamage());
   * player.setLastDamageCause(modifiedDamageEvent);
   * </pre>
   */
  @EventHandler(ignoreCancelled = true)
  protected void onEntityDamage(EntityDamageEvent event)
  {
    if (event.getEntity() instanceof Player)
    {
      final Player player = (Player) event.getEntity();

      // For damage caused by explosions, fix damager to fix death messages.
      String shooterName = null;
      if (event.getCause() == DamageCause.ENTITY_EXPLOSION)
      {
        for (MetadataValue shooter : player.getMetadata("shooter"))
        {
          if (shooter.getOwningPlugin() == this)
          {
            shooterName = shooter.asString();
            break;
          }
        }
      }

      // Was a fireball shot?
      if (shooterName != null)
      {
        final Player shootingPlayer = getServer().getPlayer(shooterName);
        if (shootingPlayer != null)
        {
          final double damage = event.getDamage();
          event.setDamage(0);
          Runnable task = new Runnable()
          {
            @Override
            public void run()
          {
            // Attribute the damage to the fireball shooter in the next tick.
            player.damage(damage, shootingPlayer);
          }
          };
          getServer().getScheduler().runTask(this, task);
        }
      }

      MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
      if (mobPlayer.getMobType() != MobType.HUMAN)
      {
        mobPlayer.playSoundInWorld(mobPlayer.getMobType().getHurtSound());
      }
    }
  } // onEntityDamage

  // --------------------------------------------------------------------------
  /**
   * Detect when a fireball is about to explode and record the shooter.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onExplosionPrime(ExplosionPrimeEvent event)
  {
    // Approximate explosion volume as a box with the event radius.
    ArrayList<Player> targetPlayers = new ArrayList<Player>();
    float sideLength = 2 * event.getRadius();
    for (Entity target : event.getEntity().getNearbyEntities(sideLength, sideLength, sideLength))
    {
      if (target instanceof Player)
      {
        targetPlayers.add((Player) target);
      }
    }

    if (event.getEntity() instanceof Projectile)
    {
      Projectile projectile = (Projectile) event.getEntity();
      LivingEntity shooter = projectile.getShooter();
      if (shooter instanceof Player)
      {
        // Mark targetted players with the shooter name as "shooter" metadata.
        for (Player targetPlayer : targetPlayers)
        {
          targetPlayer.setMetadata("shooter", new FixedMetadataValue(this, ((Player) shooter).getName()));
        }
      }
    }
    else
    {
      // Not a projectile. Maybe a creeper or TNT.
      for (Player targetPlayer : targetPlayers)
      {
        targetPlayer.removeMetadata("shooter", this);
      }
    }
  } // onExplosionPrime

  // --------------------------------------------------------------------------
  /**
   * Handle player attacks on other entities.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onEntityDamageByEntity(EntityDamageByEntityEvent event)
  {
    double damage = event.getDamage();
    if (event.getDamager() instanceof Player)
    {
      Player attacker = (Player) event.getDamager();
      MobPlayer mobAttacker = _mobPlayerTracker.getMobPlayer(attacker);
      MobType mobType = mobAttacker.getMobType();

      // If the player is using an illegal sword, cancel the event.
      ItemStack item = attacker.getItemInHand();
      if (item != null && !mobType.canUseSword(item))
      {
        Messages.success(attacker, "As " + mobType.getDisplayName() + " you can't use that sword.");
        event.setCancelled(true);
        return;
      }

      // damage = mobAttacker.adjustInflictedDamage(event.getCause(), damage,
      // attacker.getItemInHand());

      // Apply detrimental potions to the attacked entity.
      if (event.getEntity() instanceof LivingEntity)
      {
        LivingEntity defender = (LivingEntity) event.getEntity();
        for (PotionEffect potion : mobType.getAttackPotionEffects())
        {
          defender.addPotionEffect(potion, true);
        }
      }
    }
    if (event.getEntity() instanceof Player)
    {
      MobPlayer mobDefender = _mobPlayerTracker.getMobPlayer((Player) event.getEntity());
      // damage = mobDefender.adjustInflictedDamage(event.getCause(), damage,
      // null);
    }
    event.setDamage(damage);
  } // onEntityDamageByEntity

  // --------------------------------------------------------------------------
  /**
   * When a disguised player dies, make them human again.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onPlayerDeathEvent(PlayerDeathEvent event)
  {
    Player player = event.getEntity();
    MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
    if (mobPlayer.getMobType() != MobType.HUMAN)
    {
      _mobPlayerTracker.transformPlayer(mobPlayer, MobType.HUMAN, true);
      mobPlayer.playSoundInWorld(mobPlayer.getMobType().getDeathSound());
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Handle attempts to shoot a bow.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onEntityShootBow(EntityShootBowEvent event)
  {
    if (event.getEntity() instanceof Player)
    {
      Player player = (Player) event.getEntity();
      MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
      MobType mobType = mobPlayer.getMobType();
      if (!mobType.canUseBow())
      {
        Messages.success(player, "As " + mobType.getDisplayName() + " you can't use a bow.");
        event.setCancelled(true);
      }
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Prevent the player from putting on armour items forbidden by the
   * {@link MobType}.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onInventoryClose(InventoryCloseEvent event)
  {
    try
    {
      if (event.getView() != null && event.getView().getType() == InventoryType.CRAFTING && event.getPlayer() instanceof Player)
      {
        Player player = (Player) event.getPlayer();
        if (player != null)
        {
          MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
          // Should not happen, but this event has been crashing oddly.
          if (mobPlayer != null)
          {
            MobType mobType = mobPlayer.getMobType();
            if (mobType != MobType.HUMAN)
            {
              checkArmour(player, mobType);
            }
          }
        }

      }
    }
    catch (Exception ex)
    {
      getLogger().severe(ex.getClass().getName() + " in onInventoryClose()");
    }
  } // onInventoryClose

  // --------------------------------------------------------------------------
  /**
   * Called when the player consumes a food item.
   * 
   * Set up to return to human form when the healing item (golden apple) is
   * eaten.
   */
  @EventHandler(ignoreCancelled = true)
  protected void onPlayerItemConsume(PlayerItemConsumeEvent event)
  {
    if (event.getItem().getType() == _configuration.getHealingItem())
    {
      MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(event.getPlayer());
      if (mobPlayer.getMobType() != MobType.HUMAN)
      {
        _mobPlayerTracker.transformPlayer(mobPlayer, MobType.HUMAN, true);
      }
    }
  }

  // --------------------------------------------------------------------------
  /**
   * A timer callback that is called every ONE_SECOND_TICKS ticks (1 second).
   * 
   * Do things like update the experience bar on transformed players to show
   * current power level.
   */
  @Override
  public void run()
  {
    for (Player player : Bukkit.getServer().getOnlinePlayers())
    {
      MobPlayer mobPlayer = _mobPlayerTracker.getMobPlayer(player);
      MobType mobType = mobPlayer.getMobType();

      // Damage by water?
      if (mobType.isHurtByWater() &&
          (EnvironmentHelper.isRaining(player.getLocation()) ||
           EnvironmentHelper.waterExists(player.getLocation(), 3)))
      {
        // Don't send a damage event if the player is already dead. :)
        if (player.getHealth() > 0)
        {
          player.damage(mobType.getWaterDamagePerSecond());
          mobType.onWaterDamage(mobPlayer);
        }
      }

      // Damage by direct sunlight?
      if (mobType.isHurtBySun())
      {
        // Allow a helmet to prevent sun damage.
        World world = player.getLocation().getWorld();
        if (EnvironmentHelper.isDay(world) &&
            player.getLocation().getBlock().getLightFromSky() == 15 &&
            player.getFireTicks() < _configuration.getSunburnTicks() &&
            !InventoryHelper.isArmour(player.getInventory().getHelmet()))
        {
          player.setFireTicks(_configuration.getSunburnTicks());
        }
      }

      // Regenerate power up to full.
      if (mobPlayer.getPower() < mobType.getFullPower())
      {
        float newPower = Math.min(mobPlayer.getPower() + mobType.getPowerRegenRate(), mobType.getFullPower());
        mobPlayer.setPower(newPower);
      }

      // Check current region. Transform the player as necessary.
      TransformationRegion oldRegion = mobPlayer.getTransformationRegion();
      TransformationRegion newRegion = _regionTracker.getTransformationRegion(player.getLocation());
      if (newRegion != oldRegion)
      {
        MobType newMobType = null;
        if (oldRegion != null && oldRegion.getLeaveMob() != null)
        {
          newMobType = _mobTypeRegistry.getMobType(oldRegion.getLeaveMob());
          if (newMobType != null)
          {
            _mobPlayerTracker.transformPlayer(mobPlayer, newMobType, true);
          }
        }
        if (newRegion != null && newRegion.getEnterMob() != null)
        {
          newMobType = _mobTypeRegistry.getMobType(newRegion.getEnterMob());
          if (newMobType != null)
          {
            _mobPlayerTracker.transformPlayer(mobPlayer, newMobType, true);
          }
        }
        mobPlayer.setTransformationRegion(newRegion);
      }

      // Check for invalid armour and tools every 5 seconds. Also, since
      // death sometimes causes a concurrent modification exception on the
      // inventory (probably DisguiseCraft), avoid this check when the player
      // is dead.
      if ((_seconds++ % 5) == 0 && player.getHealth() > 0)
      {
        checkArmour(player, mobType);
      }

      // Every IDLE_SOUND_INTERVAL seconds on average, player makes idle sound.
      if ((int) (Math.random() * IDLE_SOUND_INTERVAL) == 0)
      {
        mobPlayer.playSoundInWorld(mobType.getIdleSound());
      }
    } // for
  } // run

  // --------------------------------------------------------------------------
  /**
   * Check that the player is wearing only allowed armour.
   * 
   * Drop any armour that should not be worn.
   * 
   * @param player the player.
   * @param mobType the mob type.
   */
  protected void checkArmour(Player player, MobType mobType)
  {
    try
    {
      if (mobType != MobType.HUMAN && !player.isDead())
      {
        PlayerInventory inv = player.getInventory();
        boolean dropped = false;
        for (int slot = InventoryHelper.BOOTS_SLOT; slot <= InventoryHelper.HELMET_SLOT; ++slot)
        {
          // CTF allows non-armour items to be worn, so check isArmour().
          // That also includes a check for null.
          ItemStack armour = inv.getItem(slot);
          if (armour != null && !mobType.canWearArmour(armour))
          {
            inv.clear(slot);
            player.getLocation().getWorld().dropItem(player.getLocation(), armour);
            dropped = true;
          }
        }
        if (dropped)
        {
          Messages.success(player, "As " + mobType.getDisplayName() + " you can't wear that armour!");
        }
      }
    }
    catch (Exception ex)
    {
      getLogger().severe(ex.getClass().getName() + " in checkArmour()");
    }
  } // checkArmour

  // --------------------------------------------------------------------------
  /**
   * Number of ticks between calls to this class' run() method.
   */
  protected static final int ONE_SECOND_TICKS    = 20;

  /**
   * Idle sound once per minute on average.
   */
  protected static final int IDLE_SOUND_INTERVAL = 60;

  // --------------------------------------------------------------------------
  /**
   * Public API.
   */
  protected API              _api                = new API(this);

  /**
   * Handles configuration.
   */
  protected Configuration    _configuration      = new Configuration(this);

  /**
   * Handles commands.
   */
  protected Commands         _commands;

  /**
   * Tracks mob types of players.
   */
  protected MobPlayerTracker _mobPlayerTracker   = new MobPlayerTracker(this);

  /**
   * Tracks cuboid regions that initate transformations of players to mob types
   * and also control whether the plugin is active in the volume.
   */
  protected RegionTracker    _regionTracker      = new RegionTracker(this);

  /**
   * Tracks configured mob types.
   */
  protected MobTypeRegistry  _mobTypeRegistry    = new MobTypeRegistry();

  /**
   * Wrapper around whatever mob disguise API we happen to use.
   */
  protected MobDisguiseAPI   _mobDisguiseAPI     = new MobDisguiseAPI();

  /**
   * Number of elapsed seconds and thus the number of run() method calls.
   */
  protected int              _seconds;
} // class MobCombat
