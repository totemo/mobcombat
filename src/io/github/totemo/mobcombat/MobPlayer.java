package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.mobs.MobType;
import io.github.totemo.mobcombat.region.TransformationRegion;
import io.github.totemo.mobcombat.team.MobTeam;
import io.github.totemo.mobcombat.util.RateLimiter;

import java.io.File;
import java.io.IOException;

import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

// ----------------------------------------------------------------------------
/**
 * Represents per-player state information about team membership and current mob
 * type.
 */
public class MobPlayer
{
  /**
   * Constructor.
   * 
   * @param player offlinePlayer the player.
   */
  public MobPlayer(OfflinePlayer offlinePlayer)
  {
    _offlinePlayer = offlinePlayer;
  }

  // --------------------------------------------------------------------------
  /**
   * Save state information about this player prior to leaving the game.
   * 
   * @param directory the directory containing all of the per-player state
   *          files.
   * @throws IOException if the file cannot be saved.
   */
  public void save(File directory)
    throws IOException
  {
    File file = new File(directory, getOfflinePlayer().getName());
    YamlConfiguration config = new YamlConfiguration();
    config.set("type", getMobType().getName());
    config.save(file);
  }

  // --------------------------------------------------------------------------
  /**
   * Load state information about this player prior to joining the game.
   * 
   * @param directory the directory containing all of the per-player state
   *          files.
   * @param plugin the owning MobCombat plugin.
   * @throws IOException if the file cannot be loaded.
   */
  public void load(File directory, MobCombat plugin)
    throws IOException
  {
    File file = new File(directory, getOfflinePlayer().getName());
    if (file.canRead())
    {
      YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
      String mobTypeName = config.getString("type", MobType.HUMAN.getName());
      _mobType = plugin.getMobTypeRegistry().getMobType(mobTypeName);
    }
    plugin.getMobDisguiseAPI().setDisguise(getPlayer(), _mobType.getMob());
  }

  // --------------------------------------------------------------------------
  /**
   * Return the associated Player.
   * 
   * @return the associated Player.
   */
  public Player getPlayer()
  {
    return _offlinePlayer.getPlayer();
  }

  // --------------------------------------------------------------------------
  /**
   * Return the associated OfflinePlayer.
   * 
   * @return the associated OfflinePlayer.
   */
  public OfflinePlayer getOfflinePlayer()
  {
    return _offlinePlayer;
  }

  // --------------------------------------------------------------------------
  /**
   * Set the MobTeam to which this player belongs.
   * 
   * @parma mobTeam the team.
   */
  public void setMobTeam(MobTeam mobTeam)
  {
    if (_mobTeam != null)
    {
      _mobTeam.removePlayer(this);
    }
    _mobTeam = mobTeam;
    if (_mobTeam != null)
    {
      _mobTeam.addPlayer(this);
    }
  }

  // --------------------------------------------------------------------------

  /**
   * Return the MobTeam to which this player belongs, or null if none.
   * 
   * @return the MobTeam to which this player belongs, or null if none.
   */
  public MobTeam getMobTeam()
  {
    return _mobTeam;
  }

  // --------------------------------------------------------------------------
  /**
   * Set the type of mob this player is currently transformed into.
   * 
   * Setting a player's mob type in this way bypasses all mob transformation
   * special effects.
   * 
   * @param type the mob type.
   * @see #transform(MobType)
   */
  public void setMobType(MobType type)
  {
    _mobType = type;
  }

  // --------------------------------------------------------------------------
  /**
   * Return the type of mob that this player is, or null if not transformed.
   * 
   * @return the type of mob that this player is, or null if not transformed.
   */
  public MobType getMobType()
  {
    return _mobType;
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if the player is currently a mob.
   * 
   * @return true if the player is currently a mob.
   */
  public boolean isMob()
  {
    return _mobType != MobType.HUMAN;
  }

  // --------------------------------------------------------------------------
  /**
   * Add the specified amount of power to the mob's current power level.
   * 
   * @param power the power in fractions of XP levels. If the value is negative,
   *          the power will be decreased by that amount.
   */
  public void addPower(float power)
  {
    setPower(getPower() + power);
  }

  // --------------------------------------------------------------------------
  /**
   * Set the mob's power level.
   * 
   * A negative number will be treated as 0.
   * 
   * @param power the power in fractions of XP levels.
   */
  public void setPower(float power)
  {
    if (power < 0.0f)
    {
      power = 0.0f;
    }

    Player player = getPlayer();
    if (player != null)
    {
      player.setLevel((int) Math.floor(power));
      player.setExp(power - player.getLevel());
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Return the mob's power level.
   * 
   * Note that power uses a completely linear scale whereby the power cost of a
   * special ability is the same regardless of the player's current XP level.
   * 
   * @return the mob's power level.
   */
  public float getPower()
  {
    Player player = getPlayer();
    return (player == null) ? 0.0f : player.getLevel() + player.getExp();
  }

  // --------------------------------------------------------------------------
  /**
   * Play the specified sound at the player's world location.
   * 
   * @param sound the sound; if null, be silent.
   */
  public void playSoundInWorld(Sound sound)
  {
    World world = getPlayer().getWorld();
    if (sound != null && _soundRateLimiter.canAct(world, _mobType.getSoundCoolDown()))
    {
      world.playSound(getPlayer().getLocation(), sound, _mobType.getSoundVolume(), 1);
    }
  }

  // --------------------------------------------------------------------------
  /**
   * Return true if the mob can use its special ability again now.
   * 
   * @return true if the mob can use its special ability again now.
   */
  public boolean canUseSpecialAbility()
  {
    // RateLimiter.canAct() must be the final check.
    return _mobType.getSpecialPowerCost() < getPower()
           && _specialRateLimiter.canAct(getPlayer().getWorld(), _mobType.getSpecialCoolDown());
  }

  // --------------------------------------------------------------------------
  /**
   * Return the TransformationRegion that the player was most recently found to
   * be in.
   * 
   * @return the TransformationRegion that the player was most recently found to
   *         be in.
   */
  public TransformationRegion getTransformationRegion()
  {
    return _transformationRegion;
  }

  // --------------------------------------------------------------------------
  /**
   * Set the TransformationRegion that the player is currently in.
   * 
   * @param transformationRegion the region.
   */
  public void setTransformationRegion(TransformationRegion transformationRegion)
  {
    _transformationRegion = transformationRegion;
  }

  // --------------------------------------------------------------------------
  /**
   * The player.
   */
  protected OfflinePlayer        _offlinePlayer;

  /**
   * The team to which this player belongs.
   */
  protected MobTeam              _mobTeam;

  /**
   * The type of the mob this player is currently transformed into, or null if
   * an ordinary player.
   */
  protected MobType              _mobType            = MobType.HUMAN;

  /**
   * Limits the rate at which voluntarily initiated mob sounds will play.
   */
  protected RateLimiter          _soundRateLimiter   = new RateLimiter();

  /**
   * Limits the rate at which the special ability can be used.
   */
  protected RateLimiter          _specialRateLimiter = new RateLimiter();

  /**
   * The TransformationRegion where the player is currently located or null if
   * there is none.
   * 
   * The current region is checked once per second in MobCombat.run().
   */
  protected TransformationRegion _transformationRegion;
} // class MobPlayer
