package io.github.totemo.mobcombat;

import io.github.totemo.mobcombat.mobs.MobType;
import io.github.totemo.mobcombat.mobs.MobTypeRegistry;
import io.github.totemo.mobcombat.util.EnvironmentHelper;
import io.github.totemo.mobcombat.util.Messages;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.amoebanman.kitmaster.utilities.CommandController.CommandHandler;
import com.amoebanman.kitmaster.utilities.CommandController.SubCommandHandler;

// --------------------------------------------------------------------------
/**
 * Handles the command line.
 */
public class Commands
{
  // --------------------------------------------------------------------------
  /**
   * Constructor.
   * 
   * @param plugin the reference to the plugin.
   */
  public Commands(MobCombat plugin)
  {
    _plugin = plugin;
    _help = new Help("Help", plugin);
  }

  // --------------------------------------------------------------------------
  /**
   * This method is called if none of the subcommands matches exactly.
   * 
   * We use it to print the help messages for all commands that the user has
   * permission to use. There's not much point in showing the other commands,
   * although they can explicitly request documentation with /mobcombat help.
   */
  @CommandHandler(name = "become", permission = "mobcombat.become")
  public void onCommandBecome(Player player, String[] args)
  {
    if (!_plugin.getRegionTracker().canUseBecome(player.getLocation()))
    {
      Messages.failure(player, "You're not allowed to use that command here.");
      return;
    }

    if (args.length < 1)
    {
      Messages.failure(player, "You must specify the type of mob to become.");
      _help.showHelp(player, "become", true);
      return;
    }
    else if (args.length == 1)
    {
      if (args[0].equals("help"))
      {
        _help.showHelp(player, "become", true);
        return;
      }
      else if (args[0].equals("list"))
      {
        StringBuilder message = new StringBuilder();
        for (MobType mobType : _plugin.getMobTypeRegistry().getAllowedTypes(player))
        {
          message.append(' ');
          message.append(mobType.getName());
        }
        Messages.value(player, "Mob types you can become", ":", message.toString());
        return;
      }
      else
      {
        // Become the specified type.
        MobPlayer mobPlayer = _plugin.getMobPlayerTracker().getMobPlayer(player);
        String mobTypeName = args[0];
        MobType mobType = _plugin.getMobTypeRegistry().getMobType(mobTypeName);
        if (mobType == null)
        {
          Messages.failure(player, mobTypeName + " is not a supported mob type.");
        }
        else if (mobType.canPlayerBecome(player))
        {
          if (mobPlayer.getMobType() == mobType)
          {
            Messages.success(player, "You're already " + mobType.getDisplayName() + ".");
          }
          // Implement in terms of API to test that.
          _plugin.getAPI().setMobType(player, mobType.getName(), true);
        }
        else
        {
          Messages.failure(player, "You're not currently allowed to become " + mobType.getDisplayName() + ".");
        }
      }
    }
    else
    {
      // args.length > 1
      Messages.failure(player, "Incorrect arguments.");
      _help.showHelp(player, "become", true);
      return;
    }
  } // onCommandBecome

  // --------------------------------------------------------------------------
  /**
   * This method is called if none of the subcommands matches exactly.
   * 
   * We use it to print the help messages for all commands that the user has
   * permission to use. There's not much point in showing the other commands,
   * although they can explicitly request documentation with /mobcombat help.
   */
  @CommandHandler(name = "mobcombat", permission = "mobcombat.help")
  public void onCommandMobCombat(CommandSender sender, String[] args)
  {
    if (args.length > 0)
    {
      Messages.failure(sender, "\"" + args[0] + "\"" + " is not a valid /mobcombat subcommand.");
    }

    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eUsage summary:"));
    for (String topic : _help.getTopics())
    {
      if (sender.hasPermission("mobcombat." + topic))
      {
        _help.showHelp(sender, topic, false);
      }
    }
    sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
      "&eType /mobcombat help &d&osubcommand &efor detailed descriptions of subcommands."));
  }

  // --------------------------------------------------------------------------
  /**
   * Handle the /mobcombat help command.
   */
  @SubCommandHandler(parent = "mobcombat", name = "help", permission = "mobcombat.help")
  public void onCommandMobCombatHelp(CommandSender sender, String[] args)
  {
    if (args.length == 2)
    {
      String topic = args[1];
      if (_help.getTopics().contains(topic))
      {
        _help.showHelp(sender, topic, true);
        return;
      }
      else
      {
        Messages.failure(sender, "\"" + topic + "\" is not a valid help topic.");
        // Fall through.
      }
    }

    // In all other cases, including failure:
    _help.showHelp(sender, "help", true);
  }

  // --------------------------------------------------------------------------
  /**
   * Handle the /mobcombat reload command.
   */
  @SubCommandHandler(parent = "mobcombat", name = "reload", permission = "mobcombat.reload")
  public void onCommandMobCombatReload(CommandSender sender, String[] args)
  {
    if (args.length == 1)
    {
      _plugin.getConfiguration().load();
      Messages.success(sender, "MobCombat configuration reloaded.");
      return;
    }

    Messages.failure(sender, "Incorrect arguments.");
    _help.showHelp(sender, "reload", true);
  }

  // --------------------------------------------------------------------------
  /**
   * Handle the /mobcombat schedule [off|on] command.
   */
  @SubCommandHandler(parent = "mobcombat", name = "schedule", permission = "mobcombat.schedule")
  public void onCommandMobCombatSchedule(CommandSender sender, String[] args)
  {
    _help.showHelp(sender, "schedule", false);
    Messages.failure(sender, "Not currently implemented.");
  }

  // --------------------------------------------------------------------------
  /**
   * Handle:
   * <ul>
   * <li>/mobcombat transform player name [type]</li>
   * <li>/mobcombat transform 25% [type]</li> [type]
   */
  @SubCommandHandler(parent = "mobcombat", name = "transform", permission = "mobcombat.transform")
  public void onCommandMobCombatTransform(CommandSender sender, String[] args)
  {
    MobTypeRegistry types = _plugin.getMobTypeRegistry();
    if ((args.length == 3 || args.length == 4) && args[1].equals("player"))
    {
      Player player = Bukkit.getPlayer(args[2]);
      if (player == null)
      {
        Messages.failure(sender, "Player \"" + args[2] + "\" is not online.");
        _help.showHelp(sender, "transform", false);
        return;
      }
      MobType mobType = (args.length == 4) ? types.getMobType(args[3]) : types.chooseRandomMobType();
      if (mobType == null)
      {
        Messages.failure(sender, "\"" + args[3] + "\" is not a valid mob type.");
        _help.showHelp(sender, "transform", false);
        return;
      }

      MobPlayerTracker tracker = _plugin.getMobPlayerTracker();
      MobPlayer mobPlayer = tracker.getMobPlayer(player);
      MobType oldMobType = mobPlayer.getMobType();
      // Implement in terms of API to test that.
      _plugin.getAPI().setMobType(player, mobType.getName(), true);
      if (oldMobType == mobType)
      {
        Messages.success(sender, player.getName() + " is already " + mobType.getDisplayName() + ".");
      }
      else
      {
        Messages.success(sender, player.getName() + " became " + mobType.getDisplayName() + ".");
      }
      return;
    }

    if ((args.length == 2 || args.length == 3) && args[1].endsWith("%"))
    {
      String digits = args[1].substring(0, args[1].length() - 1);
      try
      {
        double percentage = Double.parseDouble(digits);
        if (percentage > 100)
        {
          Messages.failure(sender, "It's not possible to transform more than 100% of the players.");
          return;
        }

        // Was a MobType explicitly specified in the arguments.
        MobType argsMobType = null;
        if (args.length == 3)
        {
          argsMobType = types.getMobType(args[2]);
          if (argsMobType == null)
          {
            Messages.failure(sender, "\"" + args[2] + "\" is not a valid mob type.");
            _help.showHelp(sender, "transform", false);
            return;
          }
        }

        String mobTypeName = (argsMobType != null) ? argsMobType.getDisplayName() : "a randomly selected mob";
        Messages.success(sender, "Transforming " +
                                 percentage + "% of online players with a view of the sky into " +
                                 mobTypeName + ".");

        // Use percentage as the probability.
        int transformedPlayerCount = 0;
        StringBuilder transformedPlayerNames = new StringBuilder();
        for (Player player : Bukkit.getServer().getOnlinePlayers())
        {
          // Skip staff.
          if (!player.hasPermission("mobcombat.notransform"))
          {
            // Skip people under rooves/underground.
            Location loc = player.getLocation();
            if (Math.random() * 100 < percentage &&
                loc.getBlock().getLightFromSky() == 15 && EnvironmentHelper.isOpenToTheSky(loc))
            {
              MobType newMobType = (argsMobType != null) ? argsMobType : types.chooseRandomMobType();
              if (newMobType != null)
              {
                _plugin.getAPI().setMobType(player, newMobType.getName(), true);
                ++transformedPlayerCount;
                transformedPlayerNames.append(' ');
                transformedPlayerNames.append(player.getName());
              }
            }
          }
        } // for

        // Success.
        if (transformedPlayerCount != 0)
        {
          Messages.value(sender, "Transformed [" + transformedPlayerCount + "]", ":", transformedPlayerNames.toString());
        }
        return;
      }
      catch (NumberFormatException ex)
      {
        Messages.failure(sender, digits + " is not a valid numnber.");
        return;
      }
    }

    // On wildly incorrect arguments:
    Messages.failure(sender, "Incorrect arguments.");
    _help.showHelp(sender, "transform", true);
  } // onCommandMobCombatTransform

  // --------------------------------------------------------------------------
  /**
   * Reference to the plugin.
   */
  protected MobCombat _plugin;

  /**
   * Handles help messages.
   */
  protected Help      _help;
} // class Commands