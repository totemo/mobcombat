= Implementation Notes =
== Time and Scheduling ==

A day is 24000 ticks long.  The notional hour is World.getTime() / 1000.  There are 8 phases of the moon, corresponding to the day number modulo 8.  The day number is World.getFullTime()/24000.


== Player Events ==

I want to prevent players who have been transformed into mobs from building.  Left click should implement attack but ignore weapons in their hand and should not break blocks.  Right click should perform a special action, if valid for that mob.  For example, for endermen it should be teleport.


Possibly relevant API details:

 * BlockBreakEvent - cancel to prevent player breaking blocks
 * BlockPlaceEvent - cancel to prevent player placing blocks
 * PlayerInteractEntityEvent - fired when a player right clicks on an Entity.
 * EntityDamageEvent - stores the amount and reason for damage on an entity.  Possible mechanism for overriding the damage caused by weapons (to make it that of a fist).  Possible mechanism for generating creeper explosion damage.
 * ExplosionPrimeEvent - called prior to an explosion.
 * EntityExplodeEvent 
 * PlayerDeathEvent
 * PlayerRespawnEvent
 * PlayerJoinEvent
 * PlayerQuitEvent and PlayerKickEvent
 

== Other APIs of Interest ==

World:
 * createExplosion(x,y,z.power,setFire,breakBlocks)
 * playEffect(location,effect,data,radius)
 * playSound(location,sound,volume,pitch)
 * setThundering(boolean)
 * setStorm(boolean) - for precipitation
 * setWeatherDuration(ticks) - for current weather conditions
 * strikeLightningEffect()
 * dropItemNaturally(Location,ItemStack) - use this to drop any items given to the player on transformation if they will not fit in the player's inventory
 

Player:
 * setCompassTarget(Location) - set where the player's compass points.  This could be used to indicate the flag direction.
 * get/setFoodLevel()
 * get/setSaturation()
 * get/setExhaustion()
 * get/setAllowFlight()
 * get/setFlySpeed()
 * get/setWalkSpeed()
 * get/setScoreboard()
 * playEffect()
 * getLevel()/setLevel() - set an integer XP level.
 

