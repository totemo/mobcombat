Plugin Dependencies
===================

 * ProtocolLib: http://dev.bukkit.org/bukkit-plugins/protocollib/
 * DisguiseCraft: http://dev.bukkit.org/bukkit-plugins/disguisecraft/


Permissions
===========

 * All players:
   * Required by DisguiseCraft:
     * disguisecraft.mob.*
     * disguisecraft.mob.sheep.color.*
   * During MobCombat testing, to use the /become command:
     * mobcombat.become.*
     * mobcombat.help
     * become.mob.Blaze
     * become.mob.Enderman
     * become.mob.Ghast
     * become.mob.Skeleton
     * become.mob.Spider
     * become.mob.Wither
     * become.mob.WitherSkeleton
     * become.mob.Zombie
   * In implementation, block access to the /become command. Rely on the schedule feature.

 * Mobcombat administrators:
   * mobcombat.*

DisguiseCraft requires the disguisecraft.mob.&lt;type&gt; permission in order to disguise a player, irrespective of whether the player is being disguised via the DisguiseCraft API or a DisguiseCraft command.

The /become command allows a player to become any mob type for which he has the become.mob.&lt;type&gt; permission node.  It's not the intended normal mode of using the plugin.  It's there for testing.  In normal use, players would be scheduled to turn into mobs at night for some phases of the moon (configurable).  They'd return to human form in the morning.  Players can increase their probability of becoming a particular kind of mob by keeping the mob's talisman in their quickbar.  Doing so is, in effect, a voting mechanism for influencing mob transformation.  It could be used to guage the relative power of mobs for balancing.


Commands that Must be Blocked
=============================
DisguiseCraft requires permission nodes to allow transformation of players into mobs via the API, and therefore players have de-facto access to the DisguiseCraft commands allowing them to manage their own disguise.  These commands therefore need to be blocked:

 * /d
 * /dis
 * /disguise
 * /u
 * /undis
 * /undisguise


DisguiseCraft Configuration
===========================

 * In order to allow disguised PVP:

      disguisePVP=true
 

 * By default, DisguiseCraft does NOT preserve the disguise across logins and that is how we like it.

      quitUndisguise=true
    
    
 * To decrease lag, it may be necessary to set:

      movementUpdateThreading=true
      movementUpdateFrequency=2


 * To prevent player names from being hidden in the Tab player list when disguised:

      noTabHide=true

