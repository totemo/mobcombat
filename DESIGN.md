= Notes on the Design of MobTeam =
== Voluntary Transformation ==

There are pros and cons to allowing players the choice of what creature to turn into.

Advantages of being able to choose the creature:

 * Since mobs are effectively a weapon type or player class, players can choose the weapon most suited to the task.
 * Given that mobs cannot build, allowing players to turn back to human allows them to continue a PvE style of play.

Disadvantages of voluntary transformation:

 * It becomes impossible to tell who is on your team.  I might add the ability to show player name tags on creatures, but it ruins the surprise aspect of being a mob.
 
Overall I am leaning towards allowing the choice but with certain caveats:

 * Perhaps the server should make the initial choice for the player.  The player can subsequently choose to transform.
 * There should be a hunger cost to transforming into a creature - probably a different cost for each type.  This automatically imposes a brief cool-down period on transformation.
 * If the hunger bar value is equal to or below the transformation cost of a particular creature, the player cannot become that creature.
 * The player reverts to human form when their hunger drops below a certain level, e.g. half a drumstick.  Health level would be a bad choice for transformation back to human because it would trigger transformation during many PVP battles.
 * We might only allow players to choose to turn into creatures that they have been at least once before.

== Potential Creature Types ==

Creeper, Skeleton, Spider, Zombie, Slime, PigZombie, Enderman, CaveSpider, Blaze, LavaSlime/MagmaCube, Bat, Witch, Pig, Sheep, Cow, Chicken, Squid, Wolf, MushroomCow, SnowMan, Ocelot, VillagerGolem/IronGolem, Villager

The following creatures don't seem like a good choice: Ghast, Giant, WItherBoss, EnderDragon, Silverfish 


== Transformation Scheduling ==

Players should be automatically turned into creatures at various times.  The most logical choice is at sunset.  Players should be given a warning before it happens, e.g. a chat message like "You're starting to feel a little queasy.".  The transformation back to human would most logically occur at sunrise.  However, given the scale of the map, it's probably not possible to cover the distance between bases in a single night.  At times, it might be nice to keep players stuck as mobs on a probabilistic basis.  There should be admin commands to force players or teams to transform to a particular creature type, or in the case of teams, a randomly selected creature on a per-player or whole-team basis.


== Separation of Creature and Player Inventories ==

Giving creatures a separate inventory from that of the player would guarantee that a creature's inventory could be safely loaded with items when the player transformed.  However, since a player could voluntarily transform back into human form, this would allow a player in creature form to gather items and then lock them away in the equivalent of an ender chest.  Items should, as a matter of principle, always be at risk while carried.  If it is necessary to give a player items for use in creature form, then they should be dropped on the ground if the player doesn't have inventory space.  Preferably, rare (and highly useful) items such as enderpearls should not be given at all.  Not giving items allows other aspects of the game design to shape the item economy.

