Building
========
MobCombat declares DisguiseCraft as a soft dependency (softdepend in plugin.yml).  In order to build MobCombat, Maven needs to be able to find the DisguiseCraft JAR file.  Not being an expert on Maven, and in the absence of any obvious hints in the DisguiseCraft documentation about how to reference DisguiseCraft from a pom.xml file, I opted to use the quickest method to solve the problem I could find without having to read the whole Maven manual:

    mvn install:install-file -Dfile=DisguiseCraft.jar -DgroupId=pgDev -DartifactId=disguisecraft -Dversion=4.8 -Dpackaging=jar
